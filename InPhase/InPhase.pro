QT += quick
QT += network
QT += charts
QT += multimedia
QT += purchasing
QT += quickcontrols2
# QT += webview

CONFIG += c++11
CONFIG += app_bundle

ICON = inPhaseIcon.icns

QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.10

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    Auth/md5.cpp \
    Auth/serialprotect.cpp \
    Measure/Globals.cpp \
    Measure/Measure.cpp \
    Processing/smaart.cpp \
    Processing/winmls.cpp \
    Measure/MeasureCollection.cpp \
    Processing/processing.cpp \
    Audio/audioEngine.cpp \
    Audio/soundcardInfo.cpp \

RESOURCES += QML/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    Auth/md5.h \
    Auth/serialprotect.h \
    Measure/Globals.h \
    Measure/Measure.h \
    Processing/smaart.h \
    Processing/winmls.h \
    Measure/MeasureCollection.h \
    Processing/processing.h \
    Audio/audioEngine.h \
    Audio/soundcardInfo.h \

DISTFILES +=

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/release/ -lsndfile
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/debug/ -lsndfile
else:unix: LIBS += -L$$PWD/../../../../../../usr/local/lib/ -lsndfile

INCLUDEPATH += $$PWD/../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/release/libsndfile.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/debug/libsndfile.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/release/sndfile.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/debug/sndfile.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/libsndfile.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/release/ -lfftw3
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../usr/local/lib/debug/ -lfftw3
else:unix: LIBS += -L$$PWD/../../../../../../usr/local/lib/ -lfftw3

INCLUDEPATH += $$PWD/../../../../../../usr/local/include
DEPENDPATH += $$PWD/../../../../../../usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/release/libfftw3.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/debug/libfftw3.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/release/fftw3.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/debug/fftw3.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../../../../usr/local/lib/libfftw3.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/'../../../RT Audio/rtaudio-5.0.0/release/' -lrtaudio
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/'../../../RT Audio/rtaudio-5.0.0/debug/' -lrtaudio
else:unix: LIBS += -L$$PWD/'../../../RT Audio/rtaudio-5.0.0/' -lrtaudio

INCLUDEPATH += $$PWD/'../../../RT Audio/rtaudio-5.0.0'
DEPENDPATH += $$PWD/'../../../RT Audio/rtaudio-5.0.0'

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'../../../RT Audio/rtaudio-5.0.0/release/librtaudio.a'
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'../../../RT Audio/rtaudio-5.0.0/debug/librtaudio.a'
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/'../../../RT Audio/rtaudio-5.0.0/release/rtaudio.lib'
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/'../../../RT Audio/rtaudio-5.0.0/debug/rtaudio.lib'
else:unix: PRE_TARGETDEPS += $$PWD/'../../../RT Audio/rtaudio-5.0.0/librtaudio.a'
