#include "Globals.h"
#include <QSettings>

Globals::Globals()
{
    // Retrieving persistent Settings
    QSettings settings("WevoSoftware", "InPhase", nullptr);
    settings.beginGroup("Var");
    m_saveDir = settings.value("SaveDir").toString();
    settings.endGroup();
}

void Globals::setSaveDir(QString dir)
{
    m_saveDir = dir;

    QSettings settings("WevoSoftware", "InPhase", nullptr);
    settings.beginGroup("Var");
    settings.setValue("SaveDir", QVariant(m_saveDir));
    settings.endGroup();

    emit saveDirChanged(m_saveDir);
}

void Globals::resetSaveDir()
{
    m_saveDir = QString();
    QSettings settings("WevoSoftware", "InPhase", nullptr);
    settings.beginGroup("Var");
    settings.remove("SaveDir");
    settings.endGroup();
    emit saveDirChanged(m_saveDir);
}
