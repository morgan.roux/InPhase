#ifndef GLOBALS_H
#define GLOBALS_H

#include <QObject>

class Globals : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY( QString saveDir READ saveDir WRITE setSaveDir NOTIFY saveDirChanged)

    QString saveDir() { return m_saveDir;}
    Globals();
    QString m_saveDir;

public slots:
    void setSaveDir(QString dir);
    void resetSaveDir();
signals :
    void saveDirChanged(QString dir);
};

#endif // GLOBALS_H
