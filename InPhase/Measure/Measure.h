#ifndef MEASURE_H
#define MEASURE_H

#include <QAbstractSeries>
#include <QXYSeries>
#include <QValueAxis>
#include <QObject>
#include <QVector>
#include <QPointF>
#include <QTimer>

#include "Processing/processing.h"
#include "Audio/audioEngine.h"

// TODO : findPrevDelay / findNextDelay

static const int X_AXIS = 0;
static const int Y_AXIS = 1;
static const bool PHASE_IN = true;
static const bool PHASE_OUT = false;



class Measure : public QObject
{
    Q_OBJECT

private:
    enum class Phase
    {
        In,
        Out,
        Auto
    };

public:
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(bool autoCompute READ autoCompute WRITE setAutoCompute NOTIFY autoComputeChanged)
    Q_PROPERTY(Phase phase READ phase WRITE setPhase NOTIFY phaseChanged)
    Q_PROPERTY(bool norm READ norm WRITE setNorm NOTIFY normChanged)
    Q_PROPERTY(bool invertPhase READ invertPhase WRITE setInvertPhase NOTIFY invertPhaseChanged )
    Q_PROPERTY(bool compareAB READ compareAB WRITE setCompareAB NOTIFY compareABChanged)
    Q_PROPERTY(bool showChanges READ showChanges WRITE setShowChanges NOTIFY showChangesChanged)
    Q_PROPERTY(QVector<QString> filename MEMBER m_filename NOTIFY filenameChanged)
    Q_PROPERTY(QPointF userDelay MEMBER m_userDelay NOTIFY userDelayChanged)
    Q_PROPERTY(bool resultPhase MEMBER m_resultPhase NOTIFY resultPhaseChanged)
    Q_PROPERTY(bool isReady READ isReady NOTIFY isReadyChanged)
    Q_PROPERTY(int nsamples READ nsamples WRITE setNsamples NOTIFY nsamplesChanged)
    Q_PROPERTY(int focus MEMBER m_focus NOTIFY focusChanged)

    // TODO : why "explicit" ?
    explicit Measure(int n=0,QObject *parent = nullptr);

    QString name() const;
    bool autoCompute() const;
    Phase phase() const;
    bool norm() const;
    bool invertPhase() const;
    bool showChanges() const;
    bool compareAB() const;
    int nsamples() const;

signals:
    void nameChanged(QString name);
    void autoComputeChanged(bool autoCompute);
    void phaseChanged(Phase phase);
    void normChanged(bool norm);
    void showChangesChanged(bool showChanges);
    void compareABChanged(bool compareAB);
    void filenameChanged(QVector<QString> filename);
    void resultChanged(QPointF result);
    void userDelayChanged(QPointF userDelay);
    void resultPhaseChanged(bool phase);
    void isReadyChanged(bool ready);
    void nsamplesChanged(int samples);
    void focusChanged(int focus);
    void invertPhaseChanged(bool invertPhase);
    void drawXCorrCompleted();
    void tfSumUpdated();
    void updateUi();

public slots:
    void setName(QString name);
    void setAutoCompute(bool autoCompute);
    void setPhase(Phase phase);
    void setNorm(bool norm);
    void setInvertPhase(bool invert);
    void setShowChanges(bool showChanges);
    void setCompareAB(bool comp);
    void setNsamples(int samples);
    void setUserDelay(int x);
    void drawImpulse1(QtCharts::QAbstractSeries *series);
    void drawImpulse2(QtCharts::QAbstractSeries *series);
    void drawWindowing(QtCharts::QAbstractSeries *series);
    void drawXCorr(QtCharts::QAbstractSeries *series);
    void drawTF1(QtCharts::QAbstractSeries *series);
    void drawTF2(QtCharts::QAbstractSeries *series);
    void drawTFSum(QtCharts::QAbstractSeries *series);
    void setWindowingOffset(int start, int nImp);
    void setWindowingLength(int length, int nImp);
    void resetWindowing(int nImp);
    void loadFromFile(QString filename);
    void compute();
    void computeTFSum();
    void findBestXCorrPeak();

private:
    void fillXYSeries(QtCharts::QXYSeries *xySeries, QVector<double> &x, QVector<double> &y);
    void findPrevXCorrPeak();
    void findNextXCorrPeak();

    void updateAxis();
    bool isReady();
    void setDefaultUserDelay();

    QString m_name;                         // measure's name
    struct Options
    {
        Phase m_phase = Phase::Auto;
        bool m_autoCompute = true;
        bool m_normalize = true;
        bool m_showChanges = true;
        bool m_invertPhase = false;
        bool m_compareAB = false;           // false : left - A / true : right - B
    }options;

    QVector<QString> m_filename;            // filename of the impulse responses
    // General properties
    int m_fs;                   // sampling rate
    int m_nsamples;              // length of the impulse response
    int m_nfft;                 // = floor(m_samples/2) +1
    double m_Fprec;             // precision of the FFT = m_Fs / m_samples;

    // Data for processing : impulse response and transfert function
    QVector<Impulse> m_imp;
    QVector<TF> m_tf;
    TF m_tfSum;
    XCorr xc;
    QVector<Window> m_window;              // window for m_imp

    QPointF m_result = QPointF(0,0);
    QPointF m_userDelay = QPointF(0,0);
    QPointF m_compareDelay = QPointF(0,0);
    bool m_resultPhase = PHASE_IN;             // true : In phase, false : out of phase
    bool m_comparePhase = true;
    bool m_readyToCompute = false;
    int m_focus = 0;                       //0 : impulse 1, 1 : impulse 2
};

#endif // MEASURE_H
