#ifndef MEASURECOLLECTION_H
#define MEASURECOLLECTION_H

#include <QAbstractListModel>
#include <QVector>

class Measure;

class MeasureCollection : public QAbstractListModel
{
    Q_OBJECT

    enum Role
    {
        NameRole = Qt::UserRole,
        PointerRole,
    };

public:
    explicit MeasureCollection(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

signals:
    void newMeasureCreated(Measure* newMeasure);

public slots:
    void create();

private:
    QVector<Measure *> _measures;
};

#endif // MEASURECOLLECTION_H
