#include "Measure.h"
#include "../Processing/processing.h"
#include <QRandomGenerator>
#include <QDebug>
#include <QtWidgets/QApplication>




Measure::Measure(int n,QObject *parent) : QObject(parent)
{
    m_imp = QVector<Impulse>(2);
    m_tf = QVector<TF>(2);
    m_window = QVector<Window>(2);
    m_filename = QVector<QString>(2,"Empty");

    m_nsamples = SAMPLES_MAX;
    m_fs = SAMPLE_RATE;
    m_nfft = floor(m_nsamples/2) +1;
    m_Fprec= m_fs / m_nsamples;
    m_result = QPointF(0,0);

    m_name = "Measure set " + QString::number(n);
    options.m_autoCompute = true;
    options.m_phase = Phase::Auto;
    options.m_normalize = true;
    options.m_showChanges = true;
}



void Measure::findPrevXCorrPeak()
{
    assert(!xc.isEmpty());
}

void Measure::findNextXCorrPeak()
{
    assert(!xc.isEmpty());
}

void Measure::findBestXCorrPeak()
{
    Peak peak;

    assert(!xc.isEmpty());

    switch(options.m_phase)
    {
    case Phase::Auto :
         peak = findAbsolutePeak(xc.y);
         break;
    case Phase::In :
         peak = findMaxPeak(xc.y);
         break;
    case Phase::Out :
         peak = findMinPeak(xc.y);
         break;
    }

    m_result.setX(peak.delta - (m_nsamples - 1));
    m_result.setY(peak.pk);

    setDefaultUserDelay();

    m_resultPhase = (peak.pk > 0);
    emit resultChanged(m_result);
    emit resultPhaseChanged(m_resultPhase);

}

void Measure::computeTFSum()
{
    Impulse imp1(m_imp[0]);
    Impulse imp2(m_imp[1]);

    if(m_imp[0].isEmpty() || m_imp[1].isEmpty())
    {
        return;
    }

    if(options.m_showChanges)
    {
        // Time shift the IR
        timeShift(imp2,m_userDelay.x());

        // Out of phase ?
        if(!m_resultPhase)
            antiphase(imp2);
    }

    if(options.m_invertPhase)
    {
        antiphase(imp2);
    }

    // Compute sum after delay
    Impulse imp_sum = sum(imp1,imp2);

    fft(m_tfSum, imp_sum);

    //Display
    //emit tfSumUpdated();
}



void Measure::drawImpulse1(QtCharts::QAbstractSeries *series)
{
    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        if(m_imp[0].isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }

        QVector<double> y = m_imp[0].y;
        if(options.m_normalize)
        {
            normalize(y);
        }
        m_window[0].apply(y);

        fillXYSeries(xySeries,m_imp[0].x, y);

        if (auto axis = static_cast<QtCharts::QValueAxis*>(xySeries->attachedAxes()[Y_AXIS])) {
            // Setting axisY
            axis->setMin(options.m_normalize ? -1 : findMinPeak(m_imp[0].y).pk);
            axis->setMax(options.m_normalize ? 1 : findMaxPeak(m_imp[0].y).pk);
            axis->applyNiceNumbers();
        }
    }
}

void Measure::drawImpulse2(QtCharts::QAbstractSeries *series)
{

    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        if(m_imp[1].isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }
        QVector<double> y = m_imp[1].y;
        if(options.m_normalize)
        {
            normalize(y);
        }
        m_window[1].apply(y);

        if(options.m_showChanges)
        {
            timeShift(y,m_userDelay.x());
            if(m_resultPhase == PHASE_OUT)
            {
                antiphase(y);
            }
        }

        if(options.m_invertPhase)
        {
            antiphase(y);
        }


        fillXYSeries(xySeries,m_imp[1].x, y);
        if (auto axis = static_cast<QtCharts::QValueAxis*>(xySeries->attachedAxes()[Y_AXIS])) {
            // Setting axisY
            axis->setMin(options.m_normalize ? -1 : findMinPeak(m_imp[1].y).pk);
            axis->setMax(options.m_normalize ? 1 : findMaxPeak(m_imp[1].y).pk);
            axis->applyNiceNumbers();
        }
    }

    return;
}


void Measure::drawTF1(QtCharts::QAbstractSeries *series)
{
    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        if(m_tf[0].isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }

        fillXYSeries(xySeries,m_tf[0].f, m_tf[0].mag);
    }
}

void Measure::drawTF2(QtCharts::QAbstractSeries *series)
{
    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        if(m_tf[1].isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }

        fillXYSeries(xySeries,m_tf[1].f, m_tf[1].mag);
    }

}

void Measure::drawTFSum(QtCharts::QAbstractSeries *series)
{
    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        if(m_tfSum.isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }

        fillXYSeries(xySeries,m_tfSum.f, m_tfSum.mag);
    }
}

void Measure::fillXYSeries(QtCharts::QXYSeries *xySeries, QVector<double> &x, QVector<double> &y)
{

    if(y.isEmpty())
    {
        // Nothing to display
        xySeries->clear();
        return;
    }

    // Updating serie
    QVector<QPointF> points;
    points.reserve(x.size());
    for(int i=0; i<x.size(); i++)
    {
        points.append({x[i], y[i]});
    }
    xySeries->replace(points);

}
void Measure::drawWindowing(QtCharts::QAbstractSeries *series)
{
    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        //fillImpulse(xySeries,m_window[focus].x, m_window[focus].y);

        if(m_window[m_focus].y.isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }

        // Updating serie
        QVector<QPointF> points;
        points.reserve(m_window[m_focus].x.size());
        for(int i=0; i<m_window[m_focus].x.size(); i++)
        {
            points.append({m_window[m_focus].x[i], m_window[m_focus].y[i]});
        }
        xySeries->replace(points);
    }

    return;
}

void Measure::drawXCorr(QtCharts::QAbstractSeries *series)
{
    if(auto *xySeries = dynamic_cast<QtCharts::QXYSeries*>(series))
    {
        if(xc.isEmpty())
        {
            // Nothing to display
            xySeries->clear();
            return;
        }

        fillXYSeries(xySeries,xc.x, xc.y);

        if (auto axis = static_cast<QtCharts::QValueAxis*>(xySeries->attachedAxes()[Y_AXIS])) {
            // Setting axisY
            axis->setMin(findMinPeak(xc.y).pk);
            axis->setMax(findMaxPeak(xc.y).pk);
            axis->applyNiceNumbers();
        }

        drawXCorrCompleted();
    }

}

void Measure::setWindowingOffset(int start, int nImp)
{

    if(start > ( m_window[nImp].m_winLenght - m_window[nImp].m_winFadeOut)) {
        return;
    }

    int l = start - m_window[nImp].m_winOffset;
    QVector<double> x;

    if(l > 0)
    {
        // Setting offset longer
        for(int i=0;i<abs(l);i++) {
            m_window[nImp].y.prepend(0);
            x.append(i);
        }
        m_window[nImp].y.remove(start+m_window[nImp].m_winFadeIn+1,l);
        m_window[nImp].m_winOffset = start;
    }
    else if(l < 0)
    {
        //Setting offset shorter
        for(int i=0;i<abs(l);i++) {
            m_window[nImp].y.insert(m_window[nImp].m_winOffset+m_window[nImp].m_winFadeIn+1,1);
            x.append(i);
        }
        m_window[nImp].y.remove(0,abs(l));
        m_window[nImp].m_winOffset = start;
    }
}

void Measure::resetWindowing(int nImp)
{
    m_window[nImp] = Window();
}

void Measure::setWindowingLength(int length, int nImp)
{   // length represents the flat part of the window, without offset, fadein and fadout

    // Compute the total length
    length += m_window[nImp].m_winFadeOut;

    if(length - m_window[nImp].m_winFadeOut < m_window[nImp].m_winOffset + m_window[nImp].m_winFadeIn) {
        return;
    }

    const int l = length - m_window[nImp].m_winLenght;
    if(l>0)
    {
        // Lenght longer
        // on peut peut-etre pouvoir eviter la boucle for, comme "i" n'est pas utilisé dedans. (pas sûr)
        for(int i=0; i<abs(l); i++)
        {
            m_window[nImp].y.insert(m_window[nImp].m_winLenght - m_window[nImp].m_winFadeOut,1);
            m_window[nImp].y.removeLast();
        }
        m_window[nImp].m_winLenght = length;
    }
    else if(l<0)
    {
        // Length shorter
        for(int i=0; i<abs(l); i++)
        {
            m_window[nImp].y.remove(m_window[nImp].m_winOffset + m_window[nImp].m_winFadeIn);
            m_window[nImp].y.append(0);
        }
        m_window[nImp].m_winLenght = length;
    }
}

void Measure::loadFromFile(QString filename)
{
    // PC version :  discard "file:///"
    // std::string filenm = filename.toStdString().substr(8);

    // MAC version : discard "file://"
    filename=filename.mid(7);

    std::string filenm = filename.toStdString();
    m_filename[m_focus] = QString::fromStdString(filenm);
    emit filenameChanged(m_filename);

    // Open file and find extension
    FILE* fid = fopen(filenm.c_str(), "rb");

    if (fid == nullptr )
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Could not open a file with the the specified filename.");
        msgBox.exec();
        return;
    }

    int l = filenm.length();
    if (l<3)
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Wrong extension - Either .wmb, .wav, or .imp must be used as file extensions");
        msgBox.exec();
        fclose(fid);
        return;
    }
    string ext = filenm.substr(l-3,3);


    //------------ WMB : WinMLS ------------//
    if (ext.compare("wmb") == 0 )
    {
        WinMLS Winmls;
        int samples,i;

        if(Winmls.load(fid))
        {
            assert(Winmls.getFs() == m_fs);

            if((samples = Winmls.getLength()) > m_nsamples)
                samples = m_nsamples;

            m_imp[m_focus].clear();

            for (i =0; i<samples; i++)
            {
                m_imp[m_focus].x.append(i);
                m_imp[m_focus].y.append(static_cast<double>(Winmls.H()[i])) ;
            }
            for(; i<m_nsamples; i++)
            {
                m_imp[m_focus].x.append(i);
                m_imp[m_focus].y.append(0) ;
            }

            // normalize(m_imp[m_focus]);
        }
    }

//*
    //------------- TRF : Smaart ----------------//
    else if (ext.compare("trf") == 0 )
    {
        Smaart Smrt;
        int samples, i;

        if (Smrt.load(fid))
        {
            if (Smrt.SR() != m_fs)
            {
                //ERROR
            }
            if((samples = Smrt.Bins()) > m_nsamples)
                samples = m_nsamples;

            // Load transfert function and convert to impulse
            TF tf(Smrt.getH(),Smrt.fPrec());
            m_imp[m_focus].clear();
            m_imp[m_focus].resize(Smrt.Bins());
            ifft(m_imp[m_focus],tf);
            m_imp[m_focus].resize(m_nsamples);

            for (i =0; i<m_nsamples; i++)
            {
                m_imp[m_focus].x[i] = i;
            }

            // normalize(m_imp[m_focus]);
        }
    }
//*/

    //----------------- WAV files ----------------//
    else if (ext.compare("wav") == 0 )
    {
        SNDFILE *file;
        SF_INFO info;
        sf_count_t idx;
        double *m_y;
        int samples, i;

        if ( (file = sf_open(filenm.c_str(),SFM_READ, &info)) == nullptr)
        {
            //ERROR
        }

        if(info.samplerate != m_fs)
        {
            //ERROR
        }
        if((samples = info.frames*info.channels) > m_nsamples)
           samples = m_nsamples;

        if (samples <= 0)
        {
            //ERROR
        }

        m_imp[m_focus].clear();
        m_y = new double[m_nsamples];
        idx = sf_read_double(file,m_y,m_nsamples);
        sf_close(file);

        for (i =0; i<samples; i++)
        {
            m_imp[m_focus].x.append(i);
            m_imp[m_focus].y.append(m_y[i]) ;
        }
        for(; i<m_nsamples; i++)
        {
            m_imp[m_focus].x.append(i);
            m_imp[m_focus].y.append(static_cast<double>(0)) ;
        }

        delete m_y;
    }

    //-------------- IMP FILE -----------------//
    else if (ext.compare("imp") == 0 )
    {
        int samples;
        int i;
        m_imp[m_focus].clear();

        QVector<double> imp = readFromImpulseFile(filename);
        if ((samples = imp.size()) > m_nsamples)
        {
            samples = m_nsamples;
        }

        for (i =0; i<samples; i++)
        {
            m_imp[m_focus].x.append(i);
            m_imp[m_focus].y.append(imp[i]) ;
        }
        for(; i<m_nsamples; i++)
        {
            m_imp[m_focus].x.append(i);
            m_imp[m_focus].y.append(static_cast<double>(0)) ;
        }
    }
    //--------------- ERROR -----------------//
    else
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Wrong extension - Either .wmb / .trf / .wav must be used as file extensions");
        msgBox.exec();
        fclose(fid);
        return;
    }

    fclose(fid);

    // Creating fft
    fft(m_tf[m_focus],m_imp[m_focus]);

    return;

}

void Measure::compute()
{
    // Check if both impulses are loaded
    if(!isReady())
        return;

    // --------- Compute ------------- //

    // Initializing
    Impulse imp1(m_imp[0]);
    Impulse imp2(m_imp[1]);

    // Windowing
    m_window[0].apply(imp1);
    m_window[1].apply(imp2);

    // XCorrelation
    xCorr(imp1.y,imp2.y,xc.y);

    // Find the delay
    findBestXCorrPeak();

    computeTFSum();

    return;
}

bool Measure::isReady()
{
    bool ready = !m_imp[0].y.isEmpty() && !m_imp[1].y.isEmpty();
    if (m_readyToCompute != ready) {
        m_readyToCompute = ready;
        emit isReadyChanged(m_readyToCompute);
    }
    return ready;
}

void Measure::setDefaultUserDelay()
{
    m_userDelay.setX(m_result.x());
    m_userDelay.setY(m_result.y());
    emit userDelayChanged(m_userDelay);
}

void Measure::setName(QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(m_name);
}

void Measure::setAutoCompute(bool autoCompute)
{
    if (options.m_autoCompute == autoCompute)
        return;

    options.m_autoCompute = autoCompute;
    emit autoComputeChanged(options.m_autoCompute);
}

void Measure::setPhase(Phase phase)
{
    if (options.m_phase == phase)
        return;

    options.m_phase = phase;
    emit phaseChanged(options.m_phase);
}

void Measure::setNorm(bool norm)
{
    if (options.m_normalize == norm)
        return;

    options.m_normalize = norm;
    emit normChanged(options.m_normalize);
}

void Measure::setInvertPhase(bool invert)
{
    if (options.m_invertPhase == invert)
        return;

    options.m_invertPhase = invert;
    emit invertPhaseChanged(options.m_invertPhase);
}

void Measure::setShowChanges(bool showDelay)
{
    if (options.m_showChanges == showDelay)
        return;

    options.m_showChanges = showDelay;
    emit showChangesChanged(options.m_showChanges);
}

void Measure::setCompareAB(bool comp)
{
    if (options.m_compareAB == comp)
        return;


    options.m_compareAB = comp;
    QPointF tempDelay = m_userDelay;
    bool tempPhase = m_resultPhase;
    m_userDelay = m_compareDelay;
    m_resultPhase = m_comparePhase;
    m_compareDelay = tempDelay;
    m_comparePhase = tempPhase;

    emit compareABChanged(options.m_compareAB);
    emit userDelayChanged(m_userDelay);
    emit resultPhaseChanged(m_resultPhase);
}

void Measure::setNsamples(int samples)
{
    if(m_nsamples == samples)
        return;

    m_nsamples = samples;
    emit nsamplesChanged(m_nsamples);

}

void Measure::setUserDelay(int x)
{
    m_userDelay.setX(x);
    m_userDelay.setY(xc.y[x+m_nsamples-1]);
    emit userDelayChanged(m_userDelay);

    if(m_userDelay.y() < 0)
    {
        if(m_resultPhase == PHASE_IN )
        {
            m_resultPhase = PHASE_OUT;
            emit resultPhaseChanged(m_resultPhase);
        }
    }
    else if(m_userDelay.y() > 0)
    {
        if(m_resultPhase == PHASE_OUT )
        {
            m_resultPhase = PHASE_IN;
            emit resultPhaseChanged(m_resultPhase);
        }
    }

}

QString Measure::name() const
{
    return m_name;
}
bool Measure::autoCompute() const
{
    return options.m_autoCompute;
}
Measure::Phase Measure::phase() const
{
    return options.m_phase;
}

bool Measure::norm() const
{
    return options.m_normalize;
}

bool Measure::invertPhase() const
{
    return options.m_invertPhase;
}

bool Measure::showChanges() const
{
    return options.m_showChanges;
}

bool Measure::compareAB() const
{
    return options.m_compareAB;
}

int Measure::nsamples() const
{
    return m_nsamples;
}
