#include "MeasureCollection.h"

#include <QQmlApplicationEngine>
#include "Measure.h"


MeasureCollection::MeasureCollection(QObject *parent) : QAbstractListModel(parent)
{

}

int MeasureCollection::rowCount(const QModelIndex &parent) const
{
    if(parent.isValid())
    {
        return 0;  // no children
    }
    return _measures.size();
}

QVariant MeasureCollection::data(const QModelIndex &index, int role) const
{
    if(index.row() >= _measures.size())
    {
        return {};
    }

    const auto & measure = _measures[index.row()];
    switch (role) {
    case Qt::DisplayRole:
    case NameRole:
        return measure->name();
    case PointerRole:
        return QVariant::fromValue(measure);
    default:
        return {};
    }
}

QHash<int, QByteArray> MeasureCollection::roleNames() const
{
    return {
        {NameRole, "name"},
        {PointerRole, "object"},
    };
}

void MeasureCollection::create()
{
    beginInsertRows({}, _measures.size(), _measures.size());

    Measure * newMeasure = new Measure(_measures.size() + 1);
    _measures.append(newMeasure);
    
    endInsertRows();

    emit newMeasureCreated(newMeasure);
}
