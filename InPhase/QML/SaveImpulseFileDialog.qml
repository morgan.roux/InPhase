import QtQuick 2.0
import QtQuick.Dialogs 1.0
import com.WevoSoftware.Globals 1.0
import com.WevoSoftware.AudioEngine 1.0

FileDialog {
    title: "Please choose a file"
    folder: globals.saveDir
    selectExisting: false

    onAccepted: {
        globals.setSaveDir(folder)
        ae.computeImpulseResponse(fileUrl);
    }

}
