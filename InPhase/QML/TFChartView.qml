import QtQuick 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtCharts 2.2

import com.WevoSoftware.Measure 1.0

// TODO : improve UI and zooms
// TODO : add a switch to enable / disable windowing

Item {
    id: item
    property int scroolSpeed : 1
    property double xValue
    property double yValue

    Layout.fillWidth: true
    Layout.fillHeight: true

    function draw() {
        currentMeasure.drawTF1(tf1Serie);
        currentMeasure.drawTF2(tf2Serie);
        currentMeasure.drawTFSum(tfSumSerie);
        //updateMarkers();
    }

//    function updateMarkers() {
//        xValue = tfView.mapToValue(Qt.point(mouseArea.mouseX,mouseArea.mouseY),
//                                      ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)
//                                      ).x;
//        yValue = ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie).at(Math.round(xValue)).y;

//        if(xValue < xAxis.min || yValue < yAxis.min
//                || xValue > xAxis.max || yValue > yAxis.max
//                || impulse1Serie.count === 0)
//        {
//            coordLayout.visible = false;
//            pointerRect.visible = false;
//        }
//        else {
//            coordLayout.visible = true;
//            pointerRect.visible = true;
//        }
//    }

    Connections {
        target: currentMeasure
        onTfSumUpdated : currentMeasure.drawTFSum(tfSumSerie)
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            id: coordLayout
            visible: false
            Text {
                id: xCoordText
                height: 80
                text: "X : " + xValue.toFixed(0)
                color : Material.foreground
            }
            Text {
                id: yCoordText
                height: 80
                text: "Y : " + yValue.toFixed(2)
                color : Material.foreground
            }
        }

        ChartView {
            id: tfView
            legend.visible: false
            title: "Transfert Functions"
            Layout.fillWidth: true
            Layout.fillHeight: true
            antialiasing: true
            //theme: ChartView.ChartThemeDark
            backgroundColor: Material.background

            LogValueAxis {
               id: xAxis
               min: 20
               max: 20000
               base: 10
               minorTickCount: -1

            }
            ValueAxis {
               id: yAxis
               min: -60
               max: 10
            }
            LineSeries {
                id: tf1Serie
                useOpenGL: true
                axisX: xAxis
                axisY: yAxis
                color: "red"
            }
            LineSeries {
                id: tf2Serie
                useOpenGL: true
                axisX: xAxis
                axisY: yAxis
                color: "blue"
            }
            LineSeries {
                id: tfSumSerie
                useOpenGL: true
                axisX: xAxis
                axisY: yAxis
                color: "green"
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                hoverEnabled: true
                onClicked : {
                    if(mouse.modifiers === Qt.ShiftModifier && currentMeasure) {
                        // Edit windowing
                        var x = tfView.mapToValue(Qt.point(mouseX,mouseY),
                                                    ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)
                                                    ).x;
                        if(mouse.button === Qt.LeftButton) {
                            currentMeasure.setWindowingOffset(x, currentMeasure.focus);
                            currentMeasure.drawWindowing(windowSerie, currentMeasure.focus);
                        }
                        else if(mouse.button === Qt.RightButton){
                            currentMeasure.setWindowingLength(x, currentMeasure.focus);
                            currentMeasure.drawWindowing(windowSerie, currentMeasure.focus);
                        }
                        if(currentMeasure.autoCompute) {
                            currentMeasure.compute();
                        }
                        updateUi();
                    }
                }
                onPressed : {
                    if(mouse.button === Qt.RightButton) {
                        // Zoom In
                        zoomRect.visible = true;
                        zoomRect.x = mouse.x;
                        zoomRect.y = mouse.y;
                    }
                }

                onPositionChanged: {
                    //updateMarkers();
                    if(pressed === true && mouse.buttons === Qt.RightButton) {
                        // Draw rectangle for zoom
                        zoomRect.width = mouse.x - zoomRect.x;
                        zoomRect.height = mouse.y - zoomRect.y;
                    }
                }
                onReleased: {
                    // Apply zoom In
                    if(mouse.button === Qt.RightButton) {
                        var r = Qt.rect(zoomRect.x,zoomRect.y,zoomRect.width, zoomRect.height)
                        tfView.zoomIn(r);
                        zoomRect.visible = false;
                        zoomRect.width = 0;
                        zoomRect.height = 0;
                        zoomRect.x = 0;
                        zoomRect.y = 0;

                        //updateMarkers();
                    }
                }
                onWheel: {
                    // Scrolling
                    if(wheel.modifiers === Qt.ShiftModifier) {
                        tfView.scrollLeft(wheel.pixelDelta.x / scroolSpeed);
                        tfView.scrollUp(wheel.pixelDelta.y / scroolSpeed);
                    }
                    else // Propagate to top level
                        wheel.accepted = false
                }

                Rectangle {
                    id: zoomRect
                    color: "white"
                    opacity: 0.8
                    border.color: "grey"
                    x: 0
                    y: 0
                    width : 0
                    height: 0
                    visible: false
                }
                onDoubleClicked: {
                    if(mouse.button === Qt.RightButton) {
                        // Zoom reset
                        tfView.zoomReset();
                        yAxis.applyNiceNumbers();
                       // updateMarkers();
                    }
                }

//                Rectangle {
//                    id: pointerRect
//                    color: "white"
//                    opacity: 0.8
//                    border.color: "black"
//                    x: tfView.mapToPosition(
//                           Qt.point(xValue, yValue),
//                           ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)).x - 3;
//                    y: tfView.mapToPosition(
//                           Qt.point(xValue, yValue),
//                           ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)).y - 3;
//                    width : 6
//                    height: 6
//                    visible: true
//                }
            }
        }
    }
}
