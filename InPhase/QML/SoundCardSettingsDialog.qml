import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import com.WevoSoftware.AudioEngine 1.0


Dialog {
    title: "Soundcard settings"
    id:  dialog
    height: 300
    width: 800
    standardButtons: Dialog.NoButton
    Material.theme: Material.Dark

    function updateOutDeviceIndex(uid) {
        for(var i=0;i<outDeviceComboBox.count;i++){
            if(outDeviceModel[i].uid === uid){
                outDeviceComboBox.currentIndex = i;
                break;
            }
        }
        return;
    }
    function updateInDeviceIndex(uid) {
        for(var i=0;i<inDeviceComboBox.count;i++){
            if(inDeviceModel[i].uid === uid){
                inDeviceComboBox.currentIndex = i;
                break;
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {

            ComboBox {
                id: inDeviceComboBox
                model: inDeviceModel
                textRole: "name"
                Layout.fillWidth: true
                onCurrentIndexChanged: {
                    // Update the channel list
                    inChannelList.clear();
                    for(var i=0; i<inDeviceModel[currentIndex].maxInputs; i++)
                    {
                        inChannelList.append({ name: "Input " + (i+1)});
                    }
                    inChannel1ComboBox.currentIndex = 0;
                    inChannel2ComboBox.currentIndex = (inChannelList.count > 1 ? 1 : 0)
                }

                Connections {
                    target: ae
                    onInDeviceChanged:{updateInDeviceIndex(uid);}
                }
            }
            ComboBox {
                id: inChannel1ComboBox
                Layout.fillWidth: true
                model: inChannelList
            }
            ComboBox {
                id: inChannel2ComboBox
                Layout.fillWidth: true
                model: inChannelList
            }
            ListModel{
                id: inChannelList
            }
        }

        RowLayout {

            ComboBox {
                id: outDeviceComboBox
                model: outDeviceModel
                textRole: "name"
                Layout.fillWidth: true
                onCurrentIndexChanged:
                {
                    // Update the channel list
                    outChannelList.clear();
                    for(var i=0; i<outDeviceModel[currentIndex].maxOutputs; i++)
                    {
                        outChannelList.append({ name: "Output " + (i+1)});
                    }
                    outChannel1ComboBox.currentIndex = 0;
                    outChannel2ComboBox.currentIndex = (outChannelList.count > 1 ? 1 : 0)
                }

                Connections {
                    target: ae
                    onOutDeviceChanged:{ updateOutDeviceIndex(uid);}
                }
            }
            ComboBox {
                id: outChannel1ComboBox
                model: outChannelList
                Layout.fillWidth: true
            }
            ComboBox {
                id: outChannel2ComboBox
                model: outChannelList
                Layout.fillWidth: true
            }
            ListModel{
                id: outChannelList
            }
        }
        RowLayout {
            Button {
                text: "Save"
                Layout.fillWidth: true
                onClicked: {
                    // Save the settings
                    ae.inDevice = inDeviceModel[inDeviceComboBox.currentIndex].uid;
                    ae.outDevice = outDeviceModel[outDeviceComboBox.currentIndex].uid;
                    ae.inChannel[0] = inChannel1ComboBox.currentIndex;
                    ae.inChannel[1] = inChannel2ComboBox.currentIndex;
                    ae.outChannel[0] = outChannel1ComboBox.currentIndex;
                    ae.outChannel[1] = outChannel2ComboBox.currentIndex;
                    dialog.visible = false
                }
            }
            Button {
                text: "Cancel"
                Layout.fillWidth: true
                onClicked: dialog.visible = false
            }
        }
    }
    onVisibleChanged:  {
        if(!visible) { return }

        // Set the default devices
        updateInDeviceIndex(ae.inDevice);
        updateOutDeviceIndex(ae.outDevice);

        // Set the selected inputs / outputs
        inChannel1ComboBox.currentIndex = ae.inChannel[0];
        inChannel2ComboBox.currentIndex = ae.inChannel[1];
        outChannel1ComboBox.currentIndex = ae.outChannel[0];
        outChannel2ComboBox.currentIndex = ae.outChannel[1];
    }
}


