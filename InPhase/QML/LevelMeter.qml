import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtCharts 2.2

Item {
    property int level : -Infinity
    property int maxHeight : 50
    property int maxWidth : 20
    property int channel
    property int minValue : -100
    property int warningValue : -20
    property int clipValue : -6
    property int okMaxHeight : 20
    property int warningMaxHeight: 27
    property int clipMaxHeight : 3

    Rectangle{
        id: background
        implicitWidth: maxWidth
        implicitHeight: maxHeight
        color: "black"
    }
    Rectangle{
        id: ok
        anchors.bottom:  background.bottom
        implicitWidth: maxWidth
        implicitHeight: (Math.min(level, warningValue) - minValue)
                        * okMaxHeight / (warningValue - minValue)
        color: "green"
    }
    Rectangle{
        id: warning
        anchors.bottom:  ok.top
        implicitWidth: maxWidth
        implicitHeight: (Math.min(level,clipValue) - warningValue)
                        * warningMaxHeight / (clipValue - warningValue)
        color: "yellow"
    }
    Rectangle{
        id: clip
        anchors.bottom:  warning.top
        implicitWidth: maxWidth
        implicitHeight: (Math.min(level, 0) - clipValue)
                        * clipMaxHeight / (-clipValue)
        color: "red"
    }
}
