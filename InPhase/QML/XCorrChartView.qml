import QtQuick 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtCharts 2.2

import com.WevoSoftware.Measure 1.0

// TODO : improve UI and zooms
// TODO : add a switch to enable / disable windowing

// TODO : why is markers less effective than in ImpulseChartView ?


Item {
    id: item
    property int scroolSpeed : 1
    property double xValue
    property double yValue

    Layout.fillWidth: true
    Layout.fillHeight: true

    function draw() {
        currentMeasure.drawXCorr(xCorrSerie);
        updateMarkers();
    }

    function updateMarkers() {
        if(!currentMeasure){
            return;
        }

        xValue = xCorrView.mapToValue(Qt.point(mouseArea.mouseX,mouseArea.mouseY),
                                      xCorrSerie
                                      ).x;
        yValue = xCorrSerie.at(Math.round(xValue) + (currentMeasure.nsamples - 1)).y;


        if(xValue < xAxis.min || yValue < yAxis.min
                || xValue > xAxis.max || yValue > yAxis.max
                || xCorrSerie.count === 0)
        {
            coordLayout.visible = false;
            pointerRect.visible = false;
        }
        else {
            coordLayout.visible = true;
            pointerRect.visible = true;
        }

        if(currentMeasure.userDelay.x < xAxis.min || currentMeasure.userDelay.y < yAxis.min
                || currentMeasure.userDelay.x > xAxis.max || currentMeasure.userDelay.y > yAxis.max
                || xCorrSerie.count === 0)
        {
            selectedRect.visible = false;
        }
        else
        {
            selectedRect.x= xCorrView.mapToPosition(
                   Qt.point(currentMeasure.userDelay.x, currentMeasure.userDelay.y),
                   xCorrSerie).x - 3;
            selectedRect.y= xCorrView.mapToPosition(
                   Qt.point(currentMeasure.userDelay.x, currentMeasure.userDelay.y),
                   xCorrSerie).y - 3;
            selectedRect.visible = true;
        }
    }

    Connections {
        target: currentMeasure
        onDrawXCorrCompleted : updateMarkers();
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            id: coordLayout
            visible: false
            Text {
                id: xCoordText
                height: 80
                text: "X : " + xValue.toFixed(0)
                color : Material.foreground
            }
            Text {
                id: yCoordText
                height: 80
                text: "Y : " + yValue.toFixed(2)
                color : Material.foreground
            }
        }

        ChartView {
            id: xCorrView
            //anchors.fill: parent
            legend.visible: false
            title:  ""
            Layout.fillWidth: true
            Layout.fillHeight: true
            antialiasing: true
            //theme: ChartView.ChartThemeDark
            backgroundColor: Material.background

            ValueAxis {
               id: xAxis
               min: (!!currentMeasure) ? -(currentMeasure.nsamples-1) : ""
               max: (!!currentMeasure) ? (currentMeasure.nsamples-1) : ""

            }
            ValueAxis {
               id: yAxis
               min: -1
               max: 1
            }
            LineSeries {
                id: xCorrSerie
                useOpenGL: true
                axisX: xAxis
                axisY: yAxis
                color: "red"
            }
            MouseArea {
                id: mouseArea
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                hoverEnabled: true
                onClicked : {
                    if(mouse.button === Qt.LeftButton && mouse.modifiers === Qt.ShiftModifier) {
                        // Set selected delay
                        currentMeasure.setUserDelay(xValue)
                        selectedRect.visible = true;
                        updateMarkers();

                        // Update the impulse chart to render the new delay
                        updateUi();
                    }
                }
                onPressed : {
                    if(mouse.button === Qt.RightButton) {
                        // Zoom In
                        zoomRect.visible = true;
                        zoomRect.x = mouse.x;
                        zoomRect.y = mouse.y;
                    }
                }
                onDoubleClicked: {
                    selectedRect.x= xCorrView.mapToPosition(
                           Qt.point(currentMeasure.userDelay.x, currentMeasure.userDelay.y),
                           xCorrSerie).x - 3;
                    selectedRect.y= xCorrView.mapToPosition(
                           Qt.point(currentMeasure.userDelay.x, currentMeasure.userDelay.y),
                           xCorrSerie).y - 3;
                    if(mouse.button === Qt.RightButton) {
                        // Zoom reset
                        xCorrView.zoomReset();
                        yAxis.applyNiceNumbers();
                        updateMarkers();
                    }
                }

                onPositionChanged: {
                    updateMarkers();
                    if(pressed === true && mouse.buttons === Qt.RightButton) {
                        // Draw rectangle for zoom
                        zoomRect.width = mouse.x - zoomRect.x;
                        zoomRect.height = mouse.y - zoomRect.y;
                    }
                }
                onReleased: {
                    // Apply zoom In
                    if(mouse.button === Qt.RightButton) {
                        var r = Qt.rect(zoomRect.x,zoomRect.y,zoomRect.width, zoomRect.height)
                        xCorrView.zoomIn(r);
                        zoomRect.visible = false;
                        zoomRect.width = 0;
                        zoomRect.height = 0;
                        zoomRect.x = 0;
                        zoomRect.y = 0;
                        updateMarkers();
                    }
                }
                onWheel: {
                    // Scrolling
                    if(wheel.modifiers === Qt.ShiftModifier) {
                        xCorrView.scrollLeft(wheel.pixelDelta.x / scroolSpeed);
                        xCorrView.scrollUp(wheel.pixelDelta.y / scroolSpeed);
                    }
                    else // Propagate to top level
                        wheel.accepted = false
                }

                Rectangle {
                    // Show the region to zoom in
                    id: zoomRect
                    color: "white"
                    opacity: 0.8
                    border.color: "grey"
                    x: 0
                    y: 0
                    width : 0
                    height: 0
                    visible: false
                }
                Rectangle {
                    // Follow the mouse and stick to the plot
                    id: pointerRect
                    color: "white"
                    opacity: 0.8
                    border.color: "black"
                    x: xCorrView.mapToPosition(
                           Qt.point(xValue, yValue),
                           xCorrSerie).x - 3;
                    y: xCorrView.mapToPosition(
                           Qt.point(xValue, yValue),
                           xCorrSerie).y - 3;
                    width : 6
                    height: 6
                    visible: false
                }
                Rectangle {
                    // Show which delayed is applyed in the processing
                    id: selectedRect
                    color: "black"
                    opacity: 0.8
                    x: (!!currentMeasure) ? (
                           xCorrView.mapToPosition(
                                Qt.point(currentMeasure.userDelay.x, currentMeasure.userDelay.y),
                                xCorrSerie).x - 3
                            ) : 0
                    y: (!!currentMeasure) ? (
                            xCorrView.mapToPosition(
                                Qt.point(currentMeasure.userDelay.x, currentMeasure.userDelay.y),
                                xCorrSerie).y - 3
                            ) : 0
                    width : 6
                    height: 6
                    border.color: "black"
                    visible: false
                }
            }
        }
    }
}
