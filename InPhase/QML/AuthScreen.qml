import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.12
import com.WevoSoftware.SerialProtect 1.0

Window {
    id: win
    title: "Please Register"
    height: 300
    width: 600
    property bool authorized : false;
    modality: Qt.ApplicationModal
    // Material.theme: Material.Dark

    onClosing: {
        close.accepted = authorized
    }
    ColumnLayout{

        Label {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            text: "Serial : "
        }
        TextField{
            // Can't edit this text field, but copying is enabled
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.preferredWidth: 600
            selectByMouse: true
            text: srl.getSerial()
            onTextChanged: text = srl.getSerial()
        }

        Label {
            id : labelKey
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            text: "Please enter your activation key."
        }

        TextField {
            id : textKey
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.preferredWidth: 600
            selectByMouse: true
            placeholderText: "Activation key"
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: "Register"
            onClicked : {
                if(srl.check(textKey.text)){
                    authorized = true
                    win.close();
                }
                else {
                    labelKey.text = "Error. Please enter a valid activation key."
                }
            }
        }

        Button {
            Layout.alignment: Qt.AlignHCenter
            text: "Exit"
            onClicked : Qt.quit();
        }
    }
}
