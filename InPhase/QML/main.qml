import QtQuick 2.12
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.10
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import com.WevoSoftware.Measure 1.0
import com.WevoSoftware.AudioEngine 1.0


ApplicationWindow {

    id : appWindow
    visible: true
    title: qsTr("In Phase")

    property int globalMargin: 9
    property int globalRadius : 5
    //Material.theme: Material.Dark

    property Measure currentMeasure;

    function updateUi() {
        impulseView.draw();
        xCorrView.draw();
        currentMeasure.computeTFSum();
        tfView.draw();
    }

    function load(file) {
        // Load an impulse form file in the current measure and compute the delay
        currentMeasure.loadFromFile(file);
        if(++currentMeasure.focus === 2){
            currentMeasure.focus = 0
        }
        if(currentMeasure.autoCompute) {
            currentMeasure.compute();
        }
        updateUi();
    }

    Component.onCompleted: {
        showMaximized();
        if(!srl.isAuthorized()){
            authScreen.visible = true;
        }
    }

    onCurrentMeasureChanged: {
        if(currentMeasure) {
            updateUi();
        }
    }

    Connections {
        target: measures
        onNewMeasureCreated : {
            currentMeasure = newMeasure
            measureList.currentIndex = measureList.count -1
        }
    }
    Connections {
        target: currentMeasure
        onUpdateUi: {
            updateUi();
        }
    }
    Connections {
        target: ae
        onFilenameRequested: {
            saveImpulseDialog.open();
        }
        onMeasureCompleted : load(filename)
    }

    RowLayout {
        id: mainLayout
        focus: true
        anchors.fill: parent
        anchors.margins: appWindow.globalMargin
        Keys.onPressed:
        {
            if (event.key === Qt.Key_Left) {
                currentMeasure.setUserDelay(currentMeasure.userDelay.x - 12);   // 0,25ms @ 48000Hz
                updateUi();
            }
            else if (event.key === Qt.Key_Right) {
                currentMeasure.setUserDelay(currentMeasure.userDelay.x + 12);
                updateUi();
            }
            else if (event.key === Qt.Key_Down) {
                currentMeasure.setUserDelay(currentMeasure.userDelay.x - 480);  // 1ms @ 48000Hz
                updateUi();
            }
            else if (event.key === Qt.Key_Up) {
                currentMeasure.setUserDelay(currentMeasure.userDelay.x + 480);
                updateUi();
            }
        }

        // Left column
        Rectangle {
            color: Material.background
            border.color: Material.foreground
            border.width: 1
            radius : globalRadius
            Layout.fillHeight: true
            Layout.preferredWidth: 200
            ColumnLayout {
                anchors.fill : parent
                anchors.margins : globalMargin

                ListView {
                    // TODO : make listview flickable
                    id: measureList
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    model: measures
                    highlight: highlight
                    highlightFollowsCurrentItem: true
                    delegate: Component {

                        Text {
                            text: name
                            height: 30
                            width: parent.width
                            color : Material.foreground
                            verticalAlignment: Text.AlignVCenter
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    measureList.currentIndex = index

                                    if(object) {
                                       currentMeasure = object
                                    }
                                }
                            }
                        }
                    }
                }
                Component {
                    id: highlight
                    Rectangle {
                        color: Material.accent;
                        radius: globalRadius
                        y: measureList.currentItem.y
                        Behavior on y {
                            SpringAnimation {
                                spring: 2
                                damping: 0.2
                            }
                        }
                    }
                }

                Switch {
                    Layout.preferredHeight: 50
                    id: invertSwitch
                    text: qsTr("Invert Phase")
                    enabled: !!(currentMeasure)
                    checked: (!!currentMeasure) ? currentMeasure.invertPhase : false
                    onToggled: {
                        currentMeasure.setInvertPhase(checked);
                        updateUi();
                    }
                }
                Switch {
                    Layout.preferredHeight: 50
                    id: compareABSwitch
                    text: qsTr("Compare A / B")
                    enabled: !!(currentMeasure)
                    checked: (!!currentMeasure) ? currentMeasure.compareAB : false
                    onToggled: {
                        currentMeasure.setCompareAB(checked);
                        updateUi();
                    }
                }
                Switch {
                    Layout.preferredHeight: 50
                    id: changeSwitch
                    text: qsTr("Show changes")
                    enabled: !!(currentMeasure)
                    checked: (!!currentMeasure) ? currentMeasure.showChanges : true
                    onToggled: {
                        currentMeasure.setShowChanges(checked);
                        updateUi();
                    }
                }
                Switch {
                    Layout.preferredHeight: 50
                    id: normSwitch
                    text: qsTr("Normalize impulses")
                    enabled: false // !!(currentMeasure)
                    checked: true // (!!currentMeasure) ? currentMeasure.norm : true
                    onToggled: {
                        currentMeasure.setNorm(checked);
                        updateUi();
                    }
                }
                Button {
                    Layout.preferredHeight: 50
                    text: qsTr("Create measure set")
                    enabled : !ae.isPlaying
                    onClicked: measures.create()
                }

                RowLayout {
                    Button {
                        Layout.preferredHeight: 50
                        Layout.fillWidth: true
                        text: qsTr("Rec")
                        enabled: !!(currentMeasure) && !ae.isPlaying
                        onClicked: {
                            if(currentMeasure && ae) {
                                ae.startMeasure();
                            }
                        }
                    }
                    Button {
                        Layout.preferredHeight: 50
                        Layout.fillWidth: true
                        text: qsTr("Stop")
                        enabled: !!(currentMeasure) && ae.isPlaying
                        onClicked: {
                            if(currentMeasure && ae) {
                                ae.abortRecord();
                            }
                        }
                    }
                    Button {
                        Layout.preferredHeight: 50
                        Layout.fillWidth: true
                        text: qsTr("Test")
                        enabled: !!(currentMeasure) && !ae.isPlaying
                        onClicked: {
                            if(currentMeasure && ae) {
                                ae.startTest();
                            }
                        }
                    }
                }

                Button {
                    Layout.preferredHeight: 50
                    text: qsTr("Settings")
                    enabled : !ae.isPlaying
                    onClicked:
                        soundCardDialog.visible = true;
                }
//                Button {
//                    Layout.preferredHeight: 50
//                    text: "buuuuy"
//                    onClicked: storeWebView.visible = true;
//                }
            }
        }

        // Right column
        ColumnLayout {
//            WebView {
//                id: webView
//                Layout.minimumHeight: 100
//                Layout.fillWidth: true
//                Layout.fillHeight: true
//                url: "https://www.inphase-audio.com"
//                onLoadingChanged: {
//                    if (loadRequest.errorString)
//                        console.error(loadRequest.errorString);
//                }
//            }

            // Chart views
            Rectangle {
                color: Material.background
                border.color: Material.foreground
                border.width: 1
                radius : globalRadius
                Layout.preferredHeight: 600
                Layout.fillWidth: true
                Layout.fillHeight: true
                ScrollView {
                    anchors.fill : parent
                    anchors.margins : globalMargin

                    Flickable {
                        clip: true
                        contentHeight: 1200
                        anchors.fill : parent
                        anchors.margins : globalMargin
                        ColumnLayout {
                            anchors.fill : parent

                            ImpulseChartView {
                                id: impulseView
                                //Layout.preferredHeight: 800
                                Layout.fillWidth: true
                            }
                            XCorrChartView {
                                id: xCorrView
                                //Layout.preferredHeight: 200
                                Layout.fillWidth: true
                            }

                            TFChartView {
                                id: tfView
                                //Layout.preferredHeight: 200
                                Layout.fillWidth: true
                            }
                        }
                    }
                }
            }
            Rectangle {
                color: Material.background
                border.color: Material.foreground
                border.width: 1
                radius : globalRadius
                Layout.preferredHeight: 100
                Layout.fillWidth: true
                Layout.fillHeight: true
                ColumnLayout {
                    anchors.fill: parent
                    anchors.margins: globalMargin

                    RowLayout {
                        LevelMeter{
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Layout.preferredWidth: 30
                            id: meter0
                            Connections {
                                target: ae
                                onLevelChanged1 : meter0.level = lvl
                            }
                        }
                        LevelMeter{
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Layout.preferredWidth: 30
                            id: meter1
                            Connections {
                                target: ae
                                onLevelChanged2 : meter1.level = lvl
                            }
                        }
                        Button {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            text: qsTr("Prev")
                            onClicked: currentMeasure.findPrevXCorrPeak();
                            enabled: false //(!!currentMeasure) ? currentMeasure.isReady : false;
                        }
                        Button {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            text: qsTr("Next")
                            onClicked : currentMeasure.findNextXCorrPeak();
                            enabled: false //(!!currentMeasure) ? currentMeasure.isReady : false;
                        }
                        Button {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            text: qsTr("Best")
                            onClicked : {
                                currentMeasure.findBestXCorrPeak();
                                updateUi()
                            }
                            enabled: (!!currentMeasure) ? currentMeasure.isReady : false;
                        }

//                        Button {
//                            Layout.fillHeight: true
//                            Layout.fillWidth: true
//                            text: qsTr("Compute")
//                            onClicked : {
//                                currentMeasure.compute();
//                                updateUi();
//                            }
//                            enabled: false
//                        }
                    }
                    Text {

                        Layout.fillWidth: true
                        property int samples : (currentMeasure.showChanges) ? Math.abs(currentMeasure.userDelay.x).toFixed(0) : 0
                        text : (!!currentMeasure) ? (
                                    qsTr("Displaying impulse n°" +
                                    ((currentMeasure.userDelay.x > 0) ? "2" : "1") + " delayed by " +
                                    samples + " samples / " + (samples/48000*1000).toFixed(1) + " ms" +
                                    (!currentMeasure.resultPhase ^ !currentMeasure.invertPhase ? " IN phase." : " OUT of phase." ))
                                ) : ""
                        color: Material.foreground
                    }
                }
            }
        }
    }

    DropArea {
        anchors.fill: (!!currentMeasure) ? parent : null
        // keys: ["text/plain"]
        onEntered: {
            appWindow.color = Material.color(Material.Grey)
        }
        onExited: {
            appWindow.color = Material.background
        }
        onDropped: {
            appWindow.color = Material.background
            if (drop.hasText) {
                if (drop.proposedAction == Qt.MoveAction || drop.proposedAction == Qt.CopyAction) {
                    if(currentMeasure){
                        drop.acceptProposedAction();
                        load(drop.text);
                    }
                }
            }
        }
    }

    SaveImpulseFileDialog {
        id: saveImpulseDialog

    }

    SoundCardSettingsDialog {

        id: soundCardDialog
    }

    AuthScreen {
        id: authScreen
        visible: false
    }

//    StoreWebView {
//        id: storeWebView
//    }
}
