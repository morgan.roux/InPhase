import QtQuick 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Window 2.1
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtCharts 2.2


import com.WevoSoftware.Measure 1.0

// TODO : error when setting the window with negative value

Item {
    property int scroolSpeed : 1
    property double xValue
    property double yValue

    Layout.fillWidth: true
    Layout.fillHeight: true

    // TODO : the impulse "above" the other depends on focus

    function draw() {
        currentMeasure.drawImpulse1(impulse1Serie);
        currentMeasure.drawImpulse2(impulse2Serie);
        currentMeasure.drawWindowing(windowSerie);
        updateMarkers();
        return;
    }

    function updateMarkers() {
        if(currentMeasure)
        {
            xValue = impulseView.mapToValue(Qt.point(mouseArea.mouseX,mouseArea.mouseY),
                                          ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)
                                          ).x;
            yValue = ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie).at(Math.round(xValue)).y;

            if(xValue < xAxis.min || yValue < yAxis.min
                    || xValue > xAxis.max || yValue > yAxis.max
                    || impulse1Serie.count === 0)
            {
                coordLayout.visible = false;
                pointerRect.visible = false;
            }
            else {
                coordLayout.visible = true;
                pointerRect.visible = true;
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        Text {
            // anchors.fill: parent
            Layout.fillWidth: true
            height: 80
            text: (!!currentMeasure) ? currentMeasure.filename[0] : ""
            wrapMode: Text.WordWrap
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(currentMeasure.focus === 1) {
                        currentMeasure.focus = 0;
                        draw();
                    }
                }
            }
            color: ((currentMeasure.focus === 0) ? Material.foreground : "grey")
        }
        Text {
            //anchors.fill: parent
            Layout.fillWidth: true
            height: 80
            text: (!!currentMeasure) ? currentMeasure.filename[1] : ""
            wrapMode: Text.WordWrap
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(currentMeasure.focus === 0) {
                         currentMeasure.focus = 1;
                         draw();
                     }
                }
            }
            color : ((currentMeasure.focus === 1) ? Material.foreground : "grey")
        }
        RowLayout {
            id: coordLayout
            visible: true
            Text {
                id: xCoordText
                height: 80
                text: "X : " + xValue.toFixed(0)
                color : Material.foreground
            }
            Text {
                id: yCoordText
                height: 80
                text: "Y : " + yValue.toFixed(2)
                color : Material.foreground
            }
        }

        ChartView {
            id: impulseView
            // anchors.fill: parent
            legend.visible: false
            title: "Impulses"
            Layout.fillWidth: true
            Layout.fillHeight: true
            antialiasing: true
            // theme: ChartView.ChartThemeDark
            backgroundColor: Material.background

            ValueAxis {
               id: xAxis
               min: 0
               max: 48000
            }
            ValueAxis {
               id: yAxis
               min: -1
               max: 1
            }
            ValueAxis {
               id: ywAxis
               min: 0
               max: 1
               visible: false
            }
            LineSeries {
                id: impulse1Serie
                useOpenGL: true
                axisX: xAxis
                axisY: yAxis
                color: "red"
            }
            LineSeries {
                id: impulse2Serie
                useOpenGL: true
                axisX: xAxis
                axisY: yAxis
                color: "blue"
            }
            LineSeries {
                id: windowSerie
                useOpenGL: true
                axisX: xAxis
                axisY: ywAxis
                color: "black"
            }

            MouseArea {
                id: mouseArea
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                hoverEnabled: true
                onClicked : {
                    if(mouse.modifiers === Qt.ShiftModifier && currentMeasure) {
                        // Edit windowing
                        var x = impulseView.mapToValue(Qt.point(mouseX,mouseY),
                                                    ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)
                                                    ).x;
                        if(mouse.button === Qt.LeftButton) {
                            // Left of the window
                            currentMeasure.setWindowingOffset(x, currentMeasure.focus);
                            currentMeasure.drawWindowing(windowSerie, currentMeasure.focus);
                        }
                        else if(mouse.button === Qt.RightButton){
                            // Right of the window
                            currentMeasure.setWindowingLength(x, currentMeasure.focus);
                            currentMeasure.drawWindowing(windowSerie, currentMeasure.focus);
                        }
                        if(currentMeasure.autoCompute) {
                            currentMeasure.compute();
                        }
                        updateUi();
                    }
                }
                onPressed : {
                    if(mouse.button === Qt.RightButton) {
                        // Zoom In
                        zoomRect.visible = true;
                        zoomRect.x = mouse.x;
                        zoomRect.y = mouse.y;
                    }
                }

                onDoubleClicked: {
                    if(mouse.modifiers === Qt.ShiftModifier && currentMeasure) {
                        // Reset windowing to default
                        currentMeasure.resetWindowing(currentMeasure.focus);
                        currentMeasure.drawWindowing(windowSerie, currentMeasure.focus);

                        if(currentMeasure.autoCompute) {
                            currentMeasure.compute();
                        }
                        updateUi();
                    }
                    else if(mouse.button === Qt.RightButton) {
                        // Zoom reset
                        impulseView.zoomReset();
                        yAxis.applyNiceNumbers();
                        updateMarkers();
                    }
                }

                onPositionChanged: {
                    updateMarkers();
                    if(pressed === true && mouse.buttons === Qt.RightButton) {
                        // Draw rectangle for zoom
                        zoomRect.width = mouse.x - zoomRect.x;
                        zoomRect.height = mouse.y - zoomRect.y;
                    }
                }

                onReleased: {
                    // Apply zoom In
                    if(mouse.button === Qt.RightButton) {
                        var r = Qt.rect(zoomRect.x,zoomRect.y,zoomRect.width, zoomRect.height)
                        impulseView.zoomIn(r);
                        zoomRect.visible = false;
                        zoomRect.width = 0;
                        zoomRect.height = 0;
                        zoomRect.x = 0;
                        zoomRect.y = 0;

                        updateMarkers();
                    }
                }
                onWheel: {
                    // Scrolling
                    if(wheel.modifiers === Qt.ShiftModifier) {
                        impulseView.scrollLeft(wheel.pixelDelta.x / scroolSpeed);
                        impulseView.scrollUp(wheel.pixelDelta.y / scroolSpeed);
                    }
                    else // Propagate to top level
                        wheel.accepted = false
                }

                Rectangle {
                    // Show the region to zoom in
                    id: zoomRect
                    color: "white"
                    opacity: 0.8
                    border.color: "grey"
                    x: 0
                    y: 0
                    width : 0
                    height: 0
                    visible: false
                }
                Rectangle {
                    // Follow the mouse and stick to the plot
                    id: pointerRect
                    color: "white"
                    opacity: 0.8
                    border.color: "black"
                    x: impulseView.mapToPosition(
                           Qt.point(xValue, yValue),
                           ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)).x - 3;
                    y: impulseView.mapToPosition(
                           Qt.point(xValue, yValue),
                           ((currentMeasure.focus === 0) ? impulse1Serie : impulse2Serie)).y - 3;
                    width : 6
                    height: 6
                    visible: true
                }
            }
        }
    }
}
