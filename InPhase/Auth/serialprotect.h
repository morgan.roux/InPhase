#ifndef SERIALPROTECT_H
#define SERIALPROTECT_H

#include <QtNetwork>
#include "stdio.h"
#include "md5.h"
#include <string>
#include <QString>
#include <QSettings>

using namespace std;
static const QString PASSWORD = "InPhaseMorganRoux";

class SerialProtect : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE bool check(QString key);
    Q_INVOKABLE QString getSerial() const;
    Q_INVOKABLE bool isAuthorized();

    explicit SerialProtect(QObject *parent = nullptr);
    bool createSerial();
    bool verifyKey(QString sn, QString key);
    QString getMacAddress();
    void writeKey(QString strg);
    void unauthorize();
    void reactivateTrial();
    bool checkTrialPeriod();

private:
    QString m_serial;
    const int TrialPeriod = 15;         //number of days in Trial mode
};

#endif // SERIALPROTECT_H
