#include "serialprotect.h"

#include <QtNetwork>
#include "stdio.h"
#include "md5.h"
#include <string>
#include <QSettings>

// TODO : Utiliser QCryptographicHash plutôt qu'une implémentation custom de MD5!

bool SerialProtect::check(QString key)
{
    if( verifyKey(m_serial, key))
    {
        writeKey(key);
        return true;
    }
    return false;
}

SerialProtect::SerialProtect(QObject *parent) : QObject(parent)
{
    createSerial();
}

bool SerialProtect::createSerial()
{
    QString macAddr;

    macAddr = getMacAddress();
    if (macAddr.isEmpty())
        return false;

    m_serial = QString::fromStdString(md5(macAddr.toStdString()));

    return true;

}

bool SerialProtect::isAuthorized()
{
    QSettings settings("Wevo Audio", "In Phase");

    settings.beginGroup("Auth");

    if (verifyKey(m_serial, settings.value("ActKey").toString()) )
    {
        settings.endGroup();
        return true;
    }
    else
    {
        settings.endGroup();

        return false;
    }

}

bool SerialProtect::verifyKey(QString sn, QString key)
{
    sn.append(PASSWORD);

    if(key.compare(QString::fromStdString(md5(sn.toStdString()))) == 0)
        return true;

    return false;
}

QString SerialProtect::getMacAddress()
{
    QString str;


    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
        // Return only the first non-loopback MAC Address
        str = netInterface.humanReadableName();

        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
        {
            str = netInterface.hardwareAddress();
            if(!str.isEmpty())
                return str;
        }
    }
    return QString();

}

void SerialProtect::writeKey(QString strg)
{
    //on passe un const char* à QVariant, mais c'est un QString qui est stockée

    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");
    settings.setValue("ActKey", QVariant(strg));
    settings.endGroup();

}

void SerialProtect::unauthorize()
{
    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");
    settings.remove("ActKey");
    settings.endGroup();
}

void SerialProtect::reactivateTrial()
{
    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");
    settings.remove("Trial");
    settings.endGroup();
}

bool SerialProtect::checkTrialPeriod()
{
    //Test : Key exist ?
    QDate currentDate = QDate::currentDate();
    QVariant trialDate;

    QSettings settings("Wevo Audio", "In Phase");
    settings.beginGroup("Auth");


    if((trialDate = settings.value("Trial")).isNull())
    {
        //1st use : store the date of the 1st use
        settings.setValue("Trial", QVariant(currentDate));
        settings.endGroup();
        return true;
    }
    else
    {
        //Test if the trial expired : 1 month

        if(currentDate.daysTo(trialDate.toDate().addDays(TrialPeriod)) <= 0)
        {   //expired
            settings.endGroup();
            return false;
        }
        else
        {   //still ok
            settings.endGroup();
            return true;
        }
    }

}

QString SerialProtect::getSerial() const
{
    return m_serial;
}

