#ifndef IMPULSE_H
#define IMPULSE_H

#include <vector>
#include <string>
#include <sndfile.h>
#include <math.h>
#include <complex>
#include <fftw3.h>
#include "winmls.h"
#include "smaart.h"
#include <cassert>
#include <QVector>

static const int SAMPLES_MAX = 48000;
static const int SAMPLE_RATE = 48000;

static const int WIN_LENGHT = 20000;
static const int WIN_FADEIN = 0;
static const int WIN_FADEOUT = 3500;
static const int WIN_OFFSET = 0;

static const std::complex<double> I_complx = {0.0, 1.0};
static const double PI = std::acos(-1);

struct Peak
{
    double pk;
    long delta;
};

struct Impulse
{
    QVector<double> x;                        // time axis
    QVector<double> y;                        // amplitude

    Impulse();
    bool isEmpty() { return y.isEmpty(); }
    void clear();
    void resize(int size);

};

struct TF
{
    QVector<double> f;                        // frequency axis
    QVector<double> mag;                      // Magnitude
    QVector<double> phs;                      // Phase
    QVector<std::complex<double>> cpx;        // complex representation

    TF();
    TF(QVector<complex<double>> z, double fPrec);
    bool isEmpty() { return mag.isEmpty() || phs.isEmpty() || cpx.isEmpty(); }
    void clear();
    void resize(int size);
    void buildFreqAxis();
};

struct XCorr
{
    QVector<double> x;                        // time axis
    QVector<double> y;                        // crosscorrellation

    XCorr(int samples = SAMPLES_MAX);
    bool isEmpty() { return y.isEmpty(); }

};

class Window
{
public:
    Window(const int fadein=WIN_FADEIN,
           const int fadeout=WIN_FADEOUT,
           const int winlen=WIN_LENGHT,
           const int offset=WIN_OFFSET,
           const int samples = SAMPLES_MAX);

    void apply(Impulse &imp) {
        apply(imp.y);
    }
    void apply(QVector<double> &v) {
        for (int i=0; i<v.size(); i++)
            v[i] *= y[i];
    }

    QVector<double> x;
    QVector<double> y;

    int m_winLenght = 0;           //m_winLenght = 0 :  no windowing
    int m_winFadeIn = 0;
    int m_winFadeOut = 0;
    int m_winOffset = 0;

};

enum class ConvDirection
{
    Convolution,
    Deconvolution,
};

void xCorr(const QVector<double> &signala, const QVector<double> &signalb, QVector<double> &result);
void conv(const QVector<double> &signala, const QVector<double> &signalb, QVector<double> &result, ConvDirection direction);
void ifft(Impulse &imp, const TF &tf);
void fft(TF &tf, const Impulse &imp);
void timeShift(Impulse &imp, int delay);
void timeShift(QVector<double> &v, int delay);
void timeShift(TF &tf, double delay);
void normalize(Impulse &imp);
void normalize(QVector<double> &v);
void antiphase(Impulse &imp);
void antiphase(TF &tf);
void antiphase(QVector<double> &v);
Impulse sum(const Impulse &imp1, const Impulse &imp2);
TF sum(const TF &tf1, const TF &tf2);
TF divide(const TF &tf1, const TF &tf2);
void cpx2polar(TF &tf);
Peak findAbsolutePeak(const QVector<double> &signal);
Peak findMaxPeak(const QVector<double> &signal);
Peak findMinPeak(const QVector<double> &signal);

void writeToImpulseFile(QString filename, QVector<double> data);
QVector<double> readFromImpulseFile(QString filename);
#endif // IMPULSE_H
