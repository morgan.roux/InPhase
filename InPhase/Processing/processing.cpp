#include "processing.h"
#include <QFile>
#include <QDataStream>

using namespace std;


Impulse::Impulse()
{
    // TODO : initialize time axis

}

void Impulse::clear()
{
    x.clear();
    y.clear();
}

void Impulse::resize(int size)
{
    x.resize(size);
    y.resize(size);
}


TF::TF()
{
    // TODO : build the freq axis
}

TF::TF(QVector<complex<double>> z, double fPrec)
{
    cpx = z;

    f.clear();
    for(int i=0; i<cpx.size(); i++)
    {
        f.append(fPrec*(i+1));
    }

    cpx2polar(*this);

}
void TF::clear()
{
    f.clear();
    cpx.clear();
    mag.clear();
    phs.clear();
}

void TF::resize(int size)
{
    f.resize(size);
    cpx.resize(size);
    mag.resize(size);
    phs.resize(size);

}

void TF::buildFreqAxis()
{
    if(f.size() == 0)
    {
        return;
    }

    for(int i=0; i<f.size(); i++)
    {
        f[i] = static_cast<double>(i+1) * static_cast<double>(SAMPLE_RATE)  / static_cast<double>(2*f.size());
    }
}


XCorr::XCorr(int samples){
    x = QVector<double>(2*samples - 1);
    y = QVector<double>(2*samples - 1);

    for (int i = 0; i<samples*2 - 1; i++) {
        x[i] = double(i)-double(samples-1);
    }
}


Window::Window(const int fadein,
               const int fadeout,
               const int winlen,
               const int offset,
               const int samples) :
    m_winFadeIn(fadein),
    m_winFadeOut(fadeout),
    m_winLenght(winlen),
    m_winOffset(offset)

{
    int i;
    x.clear();
    y.clear();

    for(i=0; i<m_winOffset; i++){
        x.append(i);
        y.append(0);
    }
    for (; i<m_winFadeIn;i++) {
        x.append(i);
        y.append(0.5f - cos(PI * (double)i-m_winOffset / (double)m_winFadeIn )/2.0f);
    }
    for(; i<m_winLenght - m_winFadeOut;i++) {
        x.append(i);
        y.append(1);
    }
    for(int j= 0; i< m_winLenght; i++, j++) {
        x.append(i);
        y.append(cos(PI*(double)j / (double)m_winFadeOut)/2.0f + 0.5f);
    }
    for(; i<samples;i++) {
        x.append(i);
        y.append(0);
    }
}

// ======================================================================== //

void conv(const QVector<double> &signala, const QVector<double> &signalb, QVector<double> &result,
          ConvDirection direction)
{
    //size result = 2*N-1, but only [O -> N-1] is revelant
    //result have to be initialized already with a size of (2*N -1)

    int N = max(signala.size(),signalb.size());
    assert(result.size() >= 2*N-1);

    double* in_a = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    double* in_b = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    complex<double> * out_a = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out_b = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));

    complex<double> scale1 = 1.0/(2 * N -1); //scale for fft->ifft
    double scalea=0;
    double scaleb=0;

    fftw_plan pa = fftw_plan_dft_r2c_1d(2*N-1, in_a,reinterpret_cast<fftw_complex*>(out_a),FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, in_a, out_a, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pb = fftw_plan_dft_r2c_1d(2*N-1, in_b,reinterpret_cast<fftw_complex*>(out_b),FFTW_ESTIMATE); //fftw_plan_dft_1d(2 * N - 1, in_b, outb, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan px = fftw_plan_dft_c2r_1d(2*N-1, reinterpret_cast<fftw_complex*>(out),result.data(),FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, out, result, FFTW_BACKWARD, FFTW_ESTIMATE);

    //zeropadding to avoid circular convolution
    memset (in_a, 0, sizeof(double) * (2*N - 1));
    memcpy (in_a, signala.constData(), sizeof(double) * signala.size());
    memset (in_b, 0, sizeof(double) * (2*N - 1));
    memcpy (in_b, signalb.constData(), sizeof(double) * signalb.size());

    //FFTs
    fftw_execute(pa);
    fftw_execute(pb);

    //calcul de la norme de in_a et in_b
    for(int i=0;i<2*N-1;i++)
    {
        scalea+=in_a[i]*in_a[i];
        scaleb+=in_b[i]*in_b[i];
    }

    if(direction == ConvDirection::Convolution)
    {
        //Convolution
        for (int i = 0; i < 2 * N - 1; i++)
        {
            out[i] = out_a[i] * out_b[i];                                   //FFT(a) *  FFT(b)
            out[i] = out[i] * scale1;                                       //Normalization FFT
        //    out[i] = out[i] / complex<double>(sqrt(scalea*scaleb)) ;       //Normalization to 1
        }
    }
    else if(direction == ConvDirection::Deconvolution)
    {
        //Deconvolution
        for (int i = 0; i < 2 * N - 1; i++)
        {
            out[i] = out_a[i] / out_b[i];                                       //FFT(a) /  FFT(b)
            out[i] = out[i] * scale1;                                       //Normalization FFT
     //       out[i] = out[i] / complex<double>(sqrt(scalea/scaleb)) ;       //Normalization to 1
        }
    }

    //iFFT
    fftw_execute(px);

    //Clean
    fftw_destroy_plan(pa);
    fftw_destroy_plan(pb);
    fftw_destroy_plan(px);

    fftw_free(in_a);
    fftw_free(in_b);
    fftw_free(out);
    fftw_free(out_a);
    fftw_free(out_b);

    fftw_cleanup();

    return;
}


void xCorr(const QVector<double> &signala, const QVector<double> &signalb, QVector<double> &result)
{
    //size signala = size signalb = N
    //size result = 2*N-1

    int N = signala.size();
    assert(signala.size() == signalb.size());


    double * in_a = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));
    double* in_b = (double *) fftw_malloc(sizeof(double) * (2 * N - 1));

    complex<double> * out_a = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out_b = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));
    complex<double> * out = (complex<double> *) fftw_malloc(sizeof(complex<double>) * (2 * N - 1));

    complex<double> scale1 = 1.0/(2 * N -1); //scale for fft->ifft
    double scalea=0;
    double scaleb=0;

    fftw_plan pa = fftw_plan_dft_r2c_1d(2*N-1, in_a,reinterpret_cast<fftw_complex*>(out_a),FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, in_a, out_a, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan pb = fftw_plan_dft_r2c_1d(2*N-1, in_b,reinterpret_cast<fftw_complex*>(out_b),FFTW_ESTIMATE); //fftw_plan_dft_1d(2 * N - 1, in_b, outb, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan px = fftw_plan_dft_c2r_1d(2*N-1, reinterpret_cast<fftw_complex*>(out),result.data(),FFTW_ESTIMATE); // fftw_plan_dft_1d(2 * N - 1, out, result, FFTW_BACKWARD, FFTW_ESTIMATE);

    //zeropadding to avoid circular cross correlation and center the result
    memset (in_a, 0, sizeof(double) * (N - 1));
    memcpy (in_a + (N - 1), signala.data(), sizeof(double) * N);
    memcpy (in_b, signalb.data(), sizeof(double) * N);
    memset (in_b + N, 0, sizeof(double) * (N - 1));

    //FFTs
    fftw_execute(pa);
    fftw_execute(pb);

    //calcul de la norme de in_a et in_b
    for(int i=0;i<2*N-1;i++)
    {
        scalea+=in_a[i]*in_a[i];
        scaleb+=in_b[i]*in_b[i];
    }

    //Cross correllation
    for (int i = 0; i < 2 * N - 1; i++)
    {
        out[i] = out_a[i] * conj(out_b[i]);                                 //FFT(a) * conj( FFT(b) )
        out[i] = out[i] * scale1 / complex<double>(sqrt(scalea*scaleb));    //Normalization
    }

    //iFFT
    fftw_execute(px);

    fftw_destroy_plan(pa);
    fftw_destroy_plan(pb);
    fftw_destroy_plan(px);

    fftw_free(in_a);
    fftw_free(in_b);
    fftw_free(out);
    fftw_free(out_a);
    fftw_free(out_b);

    fftw_cleanup();

    return;
}


void fft(TF &tf, const Impulse &imp)
{
    // FFT - no scaling
    double *in;
    int samples = imp.y.size();
    const int nfft  = floor(samples/2) + 1;
    fftw_complex *out;
    fftw_plan p;

    tf.clear();
    tf.resize(nfft);
    tf.buildFreqAxis();

    in = (double*) fftw_malloc(sizeof(double) * samples);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * samples);
    p = fftw_plan_dft_r2c_1d(samples, in, out, FFTW_PRESERVE_INPUT && FFTW_MEASURE);

    memcpy(in, imp.y.constData(), sizeof(double)*samples);
    fftw_execute(p);
    memcpy(tf.cpx.data(), out, sizeof(fftw_complex) * nfft);

    cpx2polar(tf);

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);

    return;
}

void ifft(Impulse& imp, const TF &tf)
{

    fftw_complex *in;
    int samples = imp.y.size();
    const int nfft  = floor(samples/2) + 1;
    double *out;
    fftw_plan p;

    out = (double*) fftw_malloc(sizeof(double) * samples);
    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * samples);
    p = fftw_plan_dft_c2r_1d(samples, in, out, FFTW_PRESERVE_INPUT && FFTW_MEASURE);

    memcpy(in, tf.cpx.constData(), sizeof(fftw_complex) * nfft);
    fftw_execute(p);
    memcpy(imp.y.data(), out, sizeof(double)*samples);

    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);

    return;
}

void timeShift(Impulse &imp, int delay)
{
    timeShift(imp.y, delay);
}

void timeShift(QVector<double> &v, int delay)
{
    // delay represented in samples
    const int size = v.size();
    if (delay>0)
    {
        v.insert(0, delay, 0);
        v.resize(size);
    }

    if(delay<0)
    {
        delay = abs(delay);
        v.insert(v.size(), delay, 0);
        v.remove(0,delay);
    }

}

void timeShift(TF &tf, double delay){

    // delay represented in seconds

    // multiplying by exp(2π.f.delay)
    const int nfft = tf.f.size();
    for(int i=0;i<nfft;i++) {
        tf.cpx[i] *= std::exp(-2*PI*I_complx * std::complex<double>(tf.f[i] * delay));
    }

    cpx2polar(tf);
}

void antiphase(QVector<double> &v)
{
    for(int i=0; i<v.size(); i++)
    {
        v[i] = -v[i];
    }
}

void normalize(QVector<double> &v)
{
    Peak pk = findAbsolutePeak(v);
    const double max = abs(pk.pk);
    if (max != 0)
    {
        for(int i=0; i< v.size(); i++) {
            v[i] /= max;
        }
    }
}

void normalize(Impulse &imp)
{
    normalize(imp.y);
}

void antiphase(Impulse &imp) {
    for (int i=0; i<imp.y.size(); i++) {
        imp.y[i] = -imp.y[i];
    }
}

Impulse sum(const Impulse &imp1, const Impulse &imp2)
{
    Impulse imp(imp1);
    imp.y.clear();

    assert(imp1.y.size() == imp2.y.size());
    for(int i=0; i<imp1.y.size();i++) {
        imp.y.append(imp1.y[i] + imp2.y[i]);
    }
    return imp;
}

TF sum(const TF &tf1, const TF &tf2)
{
    TF tf(tf1);
    tf.cpx.clear();

    assert(tf1.cpx.size() == tf2.cpx.size());
    for(int i=0; i<tf1.cpx.size();i++)
        tf.cpx.append(tf1.cpx[i] + tf2.cpx[i]);

    cpx2polar(tf);
    return tf;
}

TF divide(const TF &tf1, const TF &tf2)
{
    // TODO : warning division by 0
    TF tf(tf1);
    tf.cpx.clear();
    assert(tf1.cpx.size() == tf2.cpx.size());
    for(int i=0; i<tf1.cpx.size();i++) {
        tf.cpx.append(tf1.cpx[i] / tf2.cpx[i]);
    }
    cpx2polar(tf);
    return tf;
}

void cpx2polar(TF &tf){

    tf.mag.clear();
    tf.phs.clear();
    for (long l=0; l<tf.f.size(); l++)
    {
        tf.mag.append(20*log10(std::abs(tf.cpx[l])));
        tf.phs.append(std::arg(tf.cpx[l]));
    }
}

Peak findAbsolutePeak(const QVector<double> &signal)
{
    Peak pk;
    assert(!signal.isEmpty());

    pk.delta = 0;
    pk.pk = abs(signal[0]);

    for(int i = 1;i<signal.size();i++)
    {
        if(abs(signal[i]) > abs(pk.pk))
        {
            pk.pk = signal[i];
            pk.delta=i;
        }
    }
    return pk;
}

Peak findMaxPeak(const QVector<double> &signal)
{
    Peak pk;

    assert(!signal.isEmpty());

    pk.delta = 0;
    pk.pk = signal[0];

    for(int i = 1;i<signal.size();i++)
    {
        if(signal[i] > pk.pk)
        {
            pk.pk = signal[i];
            pk.delta=i;
        }
    }
    return pk;
}

Peak findMinPeak(const QVector<double> &signal)
{
    Peak pk;

    assert(!signal.isEmpty());

    pk.delta = 0;
    pk.pk = signal[0];
    for(int i = 1;i<signal.size();i++)
    {
        if(signal[i] < pk.pk)
        {
            pk.pk = signal[i];
            pk.delta=i;
        }
    }
    return pk;
}

void writeToImpulseFile(QString filename, QVector<double> data)
{
    // TODO : test if writing file is ok

    QFile file(filename);
    // If file exist : remove it for update
    if (file.exists()) { file.remove(); }

    //Writing to file
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream stream(&file);
        //stream << SAMPLE_RATE;
        stream << data;
        file.close();
    }
}

QVector<double> readFromImpulseFile(QString filename)
{
    QFile file(filename);
    QVector<double> imp;
    if(file.open(QIODevice::ReadOnly))
    {
      int fs;
      QDataStream stream(&file);
      //stream >> fs;
      stream >> imp;
      file.close();

      return imp;
    }
    return QVector<double>();
}
