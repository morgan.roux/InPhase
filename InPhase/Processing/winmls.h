#ifndef WINMLS_H
#define WINMLS_H

#include <QMainWindow>
#include <stdio.h>
#include <stdlib.h>
#include <QMessageBox>



using namespace std;



class WinMLS
{
public:
    WinMLS();
    bool load(FILE *fid);

    float *H() const;

    float getFs() const;

    int getLength() const;

private:
    static const int VersionNumber = 4;  // Reads header version 3
    static const int VersionNumber_1 = 1;	// Old files are version number 1

    char id[4];
    int Version;

    float Fs;

    char Title[80];
    char DateOfMeas[20];
    int d_new_header_exists = 0;
    int Channel = 0;
    int NumberOfChannels = 0;
    int Length = 0;
    char UseFeedbackLoop = 0;
    char Mixer = 0;


    //Input settings
    char InputDevice[30];
    int RecBitsPerSample = 0;
    int MixerInputVolume = 0;
    unsigned char MixerInputIsCalibrated = 0;
    unsigned char dummy[10];
    float MixerInputVol_db = 0;
    unsigned char HardwareInputIsCalibrated = 0;
    unsigned char InputUnitLabel[11];
    float InputConversionFactordB = 0;

    //Output settings
    unsigned char OutputDevice[30];
    int PlayBitsPerSample = 0;
    int MixerOutputMasterVolume = 0;
    int MixerOutputVolume = 0;
    int MixerOutputIsCalibrated = 0;
    float MixerOutputVol_db = 0;
    unsigned char HardwareOutputIsCalibrated = 0;
    unsigned char OutputUnitLabel[11];
    float OutputConversionFactordB = 0;

    float SpeedOfSound = 0;
    unsigned char MeasurementMode[257];
    unsigned char MeasurementSystemCorrection = 0;
    unsigned char  PreEmphasis = 0;
    unsigned char DeEmphasis = 0;
    unsigned char EmphasisFileName[256];
    int SeqOrder = 0;
    int AvgNo = 0;
    int NumberOfPreMLS = 0;
    unsigned char TypeOfMLS[11];

    //Version 4
    unsigned char MeasID[16];
    int ExtraHeaderSize = 0;
    unsigned char IsTransferfunction = 0;
    unsigned char SpecifyImpLen = 0;
    unsigned char Resample = 0;
    double SweepLenInSec = 0;
    double StartFreq = 0;
    double EndFreq = 0;
    unsigned char HPfiltered = 0;

    float* h;

};

#endif // WINMLS_H
