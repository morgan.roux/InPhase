#include "smaart.h"
using namespace std;

Smaart::Smaart()
{


}

bool Smaart::load(FILE* fid)
{/*
    FILE* fid = NULL;
    fid = fopen(filenm.c_str(), "rb");

    if (fid == NULL )
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Could not open a file with the the specified filename.");
        msgBox.exec();
        return false;
    }


     //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     //% FIND EXTENSION
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    int l = filenm.length();

     if (l<3) // check that length of string is larger than three.
    {
         QMessageBox msgBox;
        msgBox.setText("Error. Wrong extension - Either .WMB or .WMT must be used as file extensions");
        msgBox.exec();
        fclose(fid);
        return false ;
    }


    string ext = filenm.substr(l-3,3);


    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //% READ BINARY DATA FROM FILE
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if (ext.compare("trf") != 0 )
    {
           QMessageBox msgBox;
           msgBox.setText("Error. The file extension you specified is not supported. '.tff' must be used as file extensions,");
           msgBox.exec();
           return false;
    }

*/
    fread(&BHeader, sizeof(BHeader),1,fid);
    fread(&RHeader, sizeof(RHeader),1,fid);

    Fs = RHeader.SR;

/*
    //Measurement data
    float* Frequency;
    double* Magnitude;
    double* Real;
    double* Imaginary;
    double* Coherence;
    double* PeakHold;
    double* LIR;
    double* ETC;

//*/
    m_LIR = (double*) malloc(RHeader.Bins * sizeof(double));
    fseek(fid,RHeader.LIROffset, SEEK_SET);
    fread(m_LIR,sizeof(double), RHeader.Bins,fid);

    m_real = (double*) malloc(RHeader.Bins * sizeof(double));
    fseek(fid,RHeader.RealDataOffset, SEEK_SET);
    fread(m_real,sizeof(double), RHeader.Bins,fid);

    m_imaginary = (double*) malloc(RHeader.Bins * sizeof(double));
    fseek(fid,RHeader.ImagDataOffset, SEEK_SET);
    fread(m_imaginary,sizeof(double), RHeader.Bins,fid);

    return true;

}

double *Smaart::getLIR() const
{
    return m_LIR;
}

QVector<complex<double>> Smaart::getH() const
{
//    complex<double> *H = (complex<double>*) malloc(RHeader.Bins * sizeof(complex<double>));
//    for(int i=0; i< RHeader.Bins; i++)
//    {
//        H[i] = complex<double>(m_real[i], m_imaginary[i]);
//    }
//    return H;
    QVector<complex<double>> H;
    for(int i=0; i< RHeader.Bins; i++)
    {
        H.append(complex<double>(m_real[i], m_imaginary[i]));
    }
    return H;
}

void Smaart::setLIR(double *value)
{
    m_LIR = value;
}

int Smaart::SR() const
{
    return RHeader.SR;
}

void Smaart::setSR(int value)
{
    RHeader.SR = value;
}

int Smaart::Bins() const
{
    return RHeader.Bins;
}

void Smaart::setBins(int value)
{
    RHeader.Bins = value;
}

double Smaart::fPrec()
{
    return (double) RHeader.SR / (double) RHeader.FFT;
}
