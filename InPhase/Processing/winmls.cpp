#include "winmls.h"


WinMLS::WinMLS()
{
    return;
}

bool WinMLS::load(FILE* fid)
{



//*
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Change log
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// KP08012001: Updated for version 3 headers
// LM03072001: Made it work for Matlab ver. 4
// PS31032008: Included valstr in the file



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Initialize return variables
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//h = [];
//double Fs = 0; Format = []; Comment = [];		// Fs is scalar and cannot be []

      // Read format identifier
     fread(id, sizeof(*id),4,fid);

     /*
    if (std::strcmp(id, "WMLS") != 0)
    {
        QMessageBox msgBox;
        msgBox.setText("Error. The file is not a WinMLS binary file");
        msgBox.exec();
        fclose(fid);
        return false;
    }
    */

    // Read header data, check on header version first.
     fread(&Version, sizeof(Version),1,fid);
    //if (Version ~= VersionNumber_1 & Version ~= VersionNumber)

     if ((Version<1) && (Version>VersionNumber) )
    {
        QMessageBox msgBox;
        msgBox.setText("Error. Illegal version of file");
        msgBox.exec();
        fclose(fid);
        return false;
    }

    if (Version == VersionNumber_1)
    {
        char* Comment;
        unsigned long Fs;
        unsigned long MaxRecLevel;
        unsigned long len;

        // Read version 1 header
        fread(&AvgNo, sizeof(AvgNo),1,fid);
        fread(&SeqOrder,sizeof(SeqOrder),1,fid);
        fread(&Fs,sizeof(Fs),1,fid);
        fread(&RecBitsPerSample,sizeof(RecBitsPerSample),1,fid);
        fread(&PlayBitsPerSample, sizeof(PlayBitsPerSample),1,fid);
        fread(&MaxRecLevel,sizeof(MaxRecLevel),1,fid);
        fread(&Channel,sizeof(Channel),1,fid);
        // Format=[SeqOrder AvgNo Channel MaxRecLevel RecBitsPerSample PlayBitsPerSample];

        // Read comment
        fread(&len,sizeof(len),1,fid);
        Comment = (char*) malloc((len+1) * sizeof(unsigned char));
        fread(Comment, sizeof((len+1) * sizeof(unsigned char)),1,fid);
     }
     else if (Version >=3)
     {
         char Comment[60];

         float MaxRecLevel;

          // Read version 3 header
         fread(Title,1 ,80,fid);             // Measurement title
         fread(Comment, 1,60,fid);         // Channel comment
         fread(DateOfMeas, 1,20,fid);        // Date of measurement

         fread(&d_new_header_exists,sizeof(d_new_header_exists) ,1,fid);        // For internal use
         fread(&Channel, sizeof(Channel),1,fid);					// Channel number in measurement
         fread(&NumberOfChannels, sizeof(NumberOfChannels),1,fid);           // Total number of channels in this measurement
         fread(&Fs,sizeof(Fs),1,fid);							// Sampling frequency
         fread(&Length, sizeof(Length),1, fid);                     // Length of measurement
         fread(&UseFeedbackLoop, sizeof(UseFeedbackLoop),1,fid);            // Flag: Is feedback loop used?
         fread(&Mixer, sizeof(Mixer),1,fid);						// Flag: Is WinMLS mixer used?

         // Input settings
         fread(InputDevice, sizeof(*InputDevice),30,fid);			// Name of input device
         fread(&MaxRecLevel, sizeof(MaxRecLevel),1,fid);                    // Maximum recording level
         fread(&RecBitsPerSample ,sizeof(RecBitsPerSample ),1,fid);				// Number of bits per sample
         fread(&MixerInputVolume, sizeof(MixerInputVolume),1,fid);				// Mixer input volume
         fread(&MixerInputIsCalibrated, sizeof(MixerInputIsCalibrated),1,fid);         // Flag: Set to 1 if mixer input is calibrated
         fread(dummy,3,1,fid);                          // SKIP 3 bytes of padding
         fread(&MixerInputVol_db , sizeof(MixerInputVol_db ),1,fid);				// Mixer input volume in decibels.
         fread(&HardwareInputIsCalibrated, sizeof(HardwareInputIsCalibrated),1,fid);      // Flag: Set to 1 if hardware input is calibrated
         fread(&InputUnitLabel, sizeof(*InputUnitLabel),11,fid);		// Input Unit Label
         fread(&InputConversionFactordB, sizeof(InputConversionFactordB),1,fid);		// Input Conversion Factor


         // Output settings
         fread(OutputDevice, sizeof(*OutputDevice),30,fid);          // Name of output device
         fread(dummy,2,1,fid);                          // SKIP 2 bytes of padding
         fread(&PlayBitsPerSample, sizeof(PlayBitsPerSample),1,fid);				// Number of bits per sample
         fread(&MixerOutputMasterVolume, sizeof(MixerOutputMasterVolume),1,fid);		// Mixer output master volume
         fread(&MixerOutputVolume,sizeof(MixerOutputVolume),1,fid);				// Mixer output volume
         fread(&MixerOutputIsCalibrated, sizeof(MixerOutputIsCalibrated),1,fid);		// Flag: Set to 1 if mixer output is calibrated
         fread(&MixerOutputVol_db, sizeof(MixerOutputVol_db),1,fid);				// Mixer input volume in decibels.
         fread(&HardwareOutputIsCalibrated, sizeof(HardwareOutputIsCalibrated),1,fid);     // Flag: Set to 1 if hardware output is calibrated
         fread(&OutputUnitLabel, sizeof(*OutputUnitLabel),11,fid);       // Output Unit Label
         fread(&OutputConversionFactordB, sizeof(OutputConversionFactordB),1,fid);       // Output Conversion Factor

         fread(&SpeedOfSound, sizeof(SpeedOfSound),1,fid);                   // Speed of sound in meters per second
         fread(MeasurementMode,1,257,fid);      // Measurement Mode
         fread(&MeasurementSystemCorrection, sizeof(MeasurementSystemCorrection),1,fid);    // Flag: Is measurement system correction used?
         fread(&PreEmphasis, sizeof(PreEmphasis),1,fid);                    // Flag: Is preemphasis used?
         fread(&DeEmphasis, sizeof(DeEmphasis),1,fid);						// Flag: Is deemphasis used?
         fread(EmphasisFileName,1, 256,fid);     // Name of emphasis file
         fread(&SeqOrder,sizeof(SeqOrder),1,fid);                       // Sequence order
         fread(&AvgNo, sizeof(AvgNo),1,fid);							// Number of averages
         fread(&NumberOfPreMLS, sizeof(NumberOfPreMLS),1,fid);					// Number of Pre-MLS
         fread(TypeOfMLS, sizeof(*TypeOfMLS),11,fid);			    // Type of MLS

         if (Version == 3)
             fread(dummy,1,1,fid);                      //Skip one byte of padding


         // (*) Return selected information. Change this line to alter the informating returned in the format array.
        // Format=[SeqOrder AvgNo Channel MaxRecLevel RecBitsPerSample PlayBitsPerSample];

        // read trailing informating, skip extra header
        // len_e=fread(fid,1,'ulong');							// Length of extra header
        // extra_header = fread(fid,len_e,'uchar');				// Skip extra header
     }

    // Read version 4 data
    if (Version >= 4)
    {
       fread(dummy,6,1,fid);                       // Skip one byte of padding
       fread(MeasID,sizeof(*MeasID),16,fid);              //20 MeasID = setstr(fread(fid,16,'uchar'))
       fread(&ExtraHeaderSize, sizeof(ExtraHeaderSize),1,fid);              // not used
       fread(&IsTransferfunction, sizeof(IsTransferfunction),1,fid);			// Flag: Is preemphasis used?
       fread(&SpecifyImpLen, sizeof(SpecifyImpLen),1,fid);						// Flag: Is preemphasis used?
       fread(&Resample, sizeof(Resample),1,fid);						// Flag: Is preemphasis used?
       fread(&SweepLenInSec, sizeof(SweepLenInSec),1,fid);						// Maximum recording level
       fread(&StartFreq, sizeof(StartFreq),1,fid);						// Maximum recording level
       fread(&EndFreq, sizeof(EndFreq),1,fid);						// Maximum recording level
       fread(&HPfiltered, sizeof(HPfiltered),1,fid);						// Flag: Is preemphasis used?
       if (Version == 4)
           fread(dummy, 7,1,fid);		//7 Skip 7 bytes of padding

    }

     // read impulse response
     h = (float*) malloc(Length * sizeof(float));
     fread(h, sizeof(*h), Length,fid);
     fclose(fid);


  /*
   *
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//% READ ASCII DATA FROM FILE
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  elseif strcmp(ext,'WMT')
  {
    // Read first line to check if it is a WinMLS Ascii file
    string=fgetl(fid);
    if strcmp(string, '#WinMLS datafile')~=1
    {
         disp(' '); disp('Error. The specified file either corrupt or not a WinMLS Ascii file')
         fclose(fid);
         return;
    }


     // Ignore all lines starting with '#'
     Version=str2num(valstr(fid));
     if (Version>VersionNumber)
     {
       disp('Illegal version of file');
       fclose(fid);
       return;
     }

     AvgNo=str2num(valstr(fid));
     SeqOrder=str2num(valstr(fid));
     Fs=str2num(valstr(fid));
     RecBitsPerSample=str2num(valstr(fid));
     PlayBitsPerSample=str2num(valstr(fid));
     MaxRecLevel=str2num(valstr(fid));
     Channel=str2num(valstr(fid));

     //read comment
     Comment=valstr(fid);
     Comment=Comment(2:length(Comment)-1);

     Format=[SeqOrder AvgNo Channel MaxRecLevel RecBitsPerSample PlayBitsPerSample];

    //skip all lines beginning with '#'
     string=fgetl(fid);
     while (findstr(string, '#'))
          string=fgetl(fid);


     //read impulse response
    //imp_l=2^SeqOrder-1;
    //h=zeros(1,imp_l); % initialize data
     //h(1:imp_l)=fscanf(fid,'%f',inf);
     h=fscanf(fid,'%f',inf);
     fclose(fid);
   }

   //*/
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//% THE FILE EXTENSION IS NOT SUPPORTED
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*
else
  {
         QMessageBox msgBox;
         msgBox.setText("Error. The file extension you specified is not supported. '.wmb' must be used as file extensions,");
         msgBox.exec();
         return false;
  }
*/

  return true;
}



//======================================

float *WinMLS::H() const
{
    return h;
}

float WinMLS::getFs() const
{
    return Fs;
}



int WinMLS::getLength() const
{
    return Length;
}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//% THE SUBROUTINE valstr
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*
void valstr(fid)
{
    // Skip all lines beginning with '#', return non-comment string
    string=fgetl(fid);
    s=findstr(string,'#');
    if (s==1)
          string=fgetl(fid);
}
//*/
