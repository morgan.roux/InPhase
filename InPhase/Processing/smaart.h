#ifndef SMAART_H
#define SMAART_H

#include <QMainWindow>
#include <stdio.h>
#include <stdlib.h>
#include <complex>
#include <QMessageBox>

using namespace std;

class Smaart
{
public:
    Smaart();
    bool load(FILE *fid);
    double *getLIR() const;
    QVector<complex<double> > getH() const;
    void setLIR(double *value);
    int SR() const;
    void setSR(int value);
    int Bins() const;
    void setBins(int value);
    double fPrec();

private:

    float Fs;


    //Base Header
    struct BaseHeader
    {
    //public:
        char Signature[8];
        float BaseVersion;
        int Offset;
    }BHeader;

    //Reference Header
    struct RefHeader
    {
    //public:

        float RefVersion;
        int Type;
        int Major;
        int Minor;
        int Patch;
        int Build;
        char TraceName[33];
        char CaptureTime[19];
        char GroupName[33];
        char MeasurementNamedouble[33];
        char MeasDeviceFriendlyName[33];
        char RefDeviceFriendlyName[33];
        char MeasChannelFriendlyName[33];
        char RefChannelFriendlyName[33];
        char MeasDeviceOSName[33];
        char RefDeviceOSName[33];
        int FFT;
        float MagThreshold;
        char AveragerName[33];
        //3 bytes padding
        int AverageForm;
        int SR;
        int BD;
        float CalOffset;

        float Delay;
        char DataWindow[33];
        char Comment[129];
        //2 bytes padding
        float Red;
        float Green;
        float Blue;
        int Bins;
        int LIRFFT;
        char LIRAveragerName[33];
        //3 bytes padding
        int PeakHoldms;
        bool PeakHoldAvg;
        int FreqOffset;
        int MagOffset;
        int RealDataOffset;
        int ImagDataOffset;
        int CohOffset;
        int PeakHoldOffset;
        int LIROffset;
        int ETCOffset;


    }RHeader;

    //Measurement data
    float* m_frequency;
    double* m_magnitude;
    double* m_real;
    double* m_imaginary;
    double* m_coherence;
    double* m_peakHold;
    double* m_LIR;
    double* m_ETC;

};

#endif // SMAART_H

