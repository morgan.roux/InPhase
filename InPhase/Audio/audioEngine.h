#ifndef AUDIO_H
#define AUDIO_H

#include "../Processing/processing.h"
#include "RtAudio.h"
#include "soundcardInfo.h"

static const int BUFFER_FRAMES = 512;
static int RMS_LENGTH = 512;          // in samples

class AudioEngine : public QObject
{
    Q_OBJECT

    //Q_PROPERTY(QString filename READ filename WRITE setFileName NOTIFY filenameChanged)
    Q_PROPERTY(int inDevice READ inDevice WRITE setInDevice NOTIFY inDeviceChanged)
    Q_PROPERTY(int outDevice READ outDevice WRITE setOutDevice NOTIFY outDeviceChanged)
    Q_PROPERTY(int nDevices MEMBER m_nDevices NOTIFY nDevicesChanged)
    Q_PROPERTY(QVector<int> inChannel MEMBER m_selectedInputChannel NOTIFY inChannelChanged)
    Q_PROPERTY(QVector<int> outChannel MEMBER m_selectedOutputChannel NOTIFY outChannelChanged)
    Q_PROPERTY(QVector<double> level MEMBER m_level NOTIFY levelChanged)
    Q_PROPERTY(bool isPlaying MEMBER m_isPlaying NOTIFY isPlayingChanged)

public:
    AudioEngine(QObject *parent = nullptr);

    QList<QObject*> getInDeviceList() const { return m_inDeviceList;}
    QList<QObject*> getOutDeviceList() const {return m_outDeviceList;}
    int inDevice() const { return m_selectedInputDevice; }
    int outDevice() const { return m_selectedOutputDevice; }

public slots:
    void startMeasure();
    void startTest();
    void stopMeasure();
    void abortRecord();
    void computeImpulseResponse(QString filename);
    bool writeToFile(QVector<double> data, QString filename);

    void setInDevice(int i);
    void setOutDevice(int i);

signals:
    void recordCompleted();
    void filenameRequested();
    void filenameReady();
    void measureCompleted(QString filename);

    void filenameChanged(QString filename);
    void inDeviceChanged(int uid) const;
    void outDeviceChanged(int uid) const;
    void nDevicesChanged(int nDevices) const;
    void inChannelChanged(QVector<int> inChannel) const;
    void outChannelChanged(QVector<int> outChannel) const;
    void levelChanged(QVector<double> lvl);
    void levelChanged1(double lvl);
    void levelChanged2(double lvl);
    void isPlayingChanged(bool b);

private:
    void initEngine();
    void initLevelMeters();
    void setEngine();

    static int playsweep_rec( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
                       double streamTime, RtAudioStreamStatus status, void *userData );
    static int playsweep_test( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
                       double streamTime, RtAudioStreamStatus status, void *userData );

    QVector<QVector<double>> m_rec;          // Buffer for the record
    int m_playFrameIndex = 0;
    int m_recSize;

    // Audio engine related
    RtAudio m_audioEngine;
    RtAudio::StreamParameters m_outParams;
    RtAudio::StreamParameters m_inParams;
    int m_nDevices;
    unsigned int m_bufferFrames;            //  number of samples per frame
    int m_fs;
    QVector<double> m_outMixer;
    QVector<int> m_selectedInputChannel;
    QVector<int> m_selectedOutputChannel;
    int m_selectedInputDevice;
    int m_selectedOutputDevice;

    QList<QObject*> m_inDeviceList;
    QList<QObject*> m_outDeviceList;

    // Sweep params
    int m_T, m_f1, m_f2;            // T : sweep lenght (in seconds)
    double m_R;
    double m_t, m_tinv;
    int m_sweepSize;
    QVector<double> m_sweep;        // sweep for playing
    QVector<double> invsweep;       // inverse filter

    // Windowing
    QVector<double> m_sweepWin;
    QVector<double> m_recWin;
    int m_fadeinLenght;              // in samples
    int m_fadeoutLenght;

    //LevelMeters
    QVector<double> m_level;
    double m_gain = 0;

public:
    bool m_isPlaying = false;

};

#endif // AUDIO_H
