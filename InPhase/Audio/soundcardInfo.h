#ifndef SOUNDCARDINFO_H
#define SOUNDCARDINFO_H

#include "RtAudio.h"
#include <QString>
#include <QObject>

// TODO : deal with Mac OS Roman encoding

class SoundcardInfos : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(int uid READ uid)
    Q_PROPERTY(int maxInputs READ maxInputs)
    Q_PROPERTY(int maxOutputs READ maxOutputs)
    Q_PROPERTY(int currentInput READ currentInput WRITE setCurrentInput NOTIFY currentInputChanged)
    Q_PROPERTY(int currentOutput READ currentOutput WRITE setCurrentOutput NOTIFY currentOutputChanged)

public:
    SoundcardInfos(RtAudio::DeviceInfo info, int uid);

    QString name() const { return m_name; }
    int uid() const { return m_uid;}
    int maxInputs() const { return m_deviceInfo.inputChannels; }
    int maxOutputs() const { return m_deviceInfo.outputChannels; }
    int currentInput() const { return m_currentInput; }
    int currentOutput() const { return m_currentOutput; }
    RtAudio::DeviceInfo deviceInfo() const {return m_deviceInfo;}

private:
    const RtAudio::DeviceInfo m_deviceInfo;
    QString m_name;
    int m_uid;
    int m_currentInput;
    int m_currentOutput;

signals:
    void nameChanged(QString name);
    void currentInputChanged(int i);
    void currentOutputChanged(int i);

public slots:
    void setUid(int uid);
    void setCurrentInput(int i);
    void setCurrentOutput(int i);
};

#endif // SOUNDCARDINFO_H
