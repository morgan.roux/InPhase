#include "soundcardInfo.h"
#include <QString>
#include <QTextCodec>

SoundcardInfos::SoundcardInfos(RtAudio::DeviceInfo info, int uid) :
    m_deviceInfo(info),
    m_uid(uid)
{
    //MacOSX version
    QTextCodec *codec = QTextCodec::codecForName("Apple Roman");
    m_name = codec->toUnicode(m_deviceInfo.name.c_str());
}

void SoundcardInfos::setUid(int uid)
{
    m_uid = uid;
}

void SoundcardInfos::setCurrentInput(int i)
{
    if (m_currentInput == i)
        return;

    m_currentInput = i;
    emit currentInputChanged(m_currentInput);
}

void SoundcardInfos::setCurrentOutput(int i)
{
    if (m_currentOutput == i)
        return;

    m_currentOutput = i;
    emit currentOutputChanged(m_currentOutput);

}
