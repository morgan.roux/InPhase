#include "audioEngine.h"
#include "audioEngine.h"
#include <QDebug>
#include <QtCore>
#include <QFileDialog>
#include <QCoreApplication>
#include <QDialog>
#include <complex>
#include <QString>
#include <sndfile.h>
#include <QQmlApplicationEngine>
#include <QQmlContext>


AudioEngine::AudioEngine(QObject *parent) : QObject(parent)
{
    //--- Initialize data ---//
    m_bufferFrames = BUFFER_FRAMES; //  number of samples per frame
    m_fs = SAMPLE_RATE;
    m_T=3;
    m_f1=20;
    m_f2=round(m_fs/2);              // T : longueur du sweep en seconde
    m_R = log(m_f2/m_f1);
    m_sweepSize = (int) round(m_T * m_fs);
    m_recSize = (unsigned int) round((m_T+1) * m_fs);        //On enregistre 1sec de plus

    m_fadeinLenght = floor(0.01f * m_fs);         //10 msec
    m_fadeoutLenght = floor(0.01f *m_fs);        //10 msec

    m_selectedInputChannel = QVector<int>(2);
    m_selectedOutputChannel = QVector<int>(2);

    // Init windows (hanning)
    // TODO : using private function to initialize the windows
    {
    int i;
    for (i=0; i<m_fadeinLenght;i++)
        m_sweepWin.append(0.5f - cos(PI * (double)i / (double)m_fadeinLenght )/2.0f);
    for(; i<m_sweepSize - m_fadeoutLenght;i++)
        m_sweepWin.append(1);
    for(unsigned int j= 0; i< m_sweepSize; i++, j++)
        m_sweepWin.append(cos(PI*(double)j / (double)m_fadeoutLenght)/2.0f + 0.5f);

    for (i=0; i<m_fadeinLenght;i++)
        m_recWin.append(0.5f - cos(PI * (double)i / (double)m_fadeinLenght )/2.0f);
    for(; i<m_recSize - m_fadeoutLenght;i++)
        m_recWin.append(1);
    for(unsigned int j= 0; i< m_recSize; i++, j++)
        m_recWin.append(cos(PI*(double)j / (double)m_fadeoutLenght)/2.0f + 0.5f);
    }

    // Init sweep
    for (int i=0; i<m_sweepSize; i++)
    {
         m_t = (double)i / (double)m_fs;
         m_sweep.append((double) sin((2*PI*m_f1*m_T/m_R)*(exp(m_t*m_R/m_T) - 1 )));
         m_sweep[i] *= m_sweepWin[i];                                       //Applying hanning window
    }

    // Init inverse filter
    for (int i=0; i<m_sweepSize; i++)
    {
        m_t = (double)i / (double)m_fs;
        invsweep.append(m_sweep[m_sweepSize-1-i] / exp(m_t*m_R/m_T));
    }

    initEngine();
}

void AudioEngine::initEngine()
{

    // Determine the number of devices available
    m_nDevices = m_audioEngine.getDeviceCount();

    // Scan through devices for various capabilities and load informations
    for (int i=0; i<m_nDevices; i++)
    {
        if(m_audioEngine.getDeviceInfo(i).inputChannels > 0)
        {
            m_inDeviceList.append(new SoundcardInfos(m_audioEngine.getDeviceInfo(i),i));
        }
        if(m_audioEngine.getDeviceInfo(i).outputChannels > 0)
        {
            m_outDeviceList.append(new SoundcardInfos(m_audioEngine.getDeviceInfo(i),i));
        }
    }

    // Set default device and default channels
    m_selectedInputDevice = m_audioEngine.getDefaultInputDevice();
    m_selectedOutputDevice = m_audioEngine.getDefaultOutputDevice();
    emit inDeviceChanged(m_selectedInputDevice);
    emit outDeviceChanged(m_selectedOutputDevice);

    m_selectedInputChannel[0] = 0;
    m_selectedInputChannel[1] = 1;
    m_selectedOutputChannel[0] = 0;
    m_selectedOutputChannel[1] = 1;

    connect(this, &AudioEngine::recordCompleted,
            this, &AudioEngine::stopMeasure);

}

void AudioEngine::setEngine()
{
    m_isPlaying = true;
    emit isPlayingChanged(m_isPlaying);

    //Setting params
    m_outParams.deviceId = m_selectedOutputDevice;
    m_outParams.nChannels = m_audioEngine.getDeviceInfo(m_outParams.deviceId).outputChannels;
    m_outParams.firstChannel = 0;

    m_inParams.deviceId = m_selectedInputDevice;
    m_inParams.nChannels = m_audioEngine.getDeviceInfo(m_inParams.deviceId).inputChannels;
    m_inParams.firstChannel = 0;

    m_rec.clear();
    m_rec = QVector<QVector<double>>(m_inParams.nChannels);      //m_rec[i] : record of input channel i

    m_level.clear();
    m_level = QVector<double>(m_inParams.nChannels);

    //Setting output mixer
    m_outMixer = QVector<double>(m_outParams.nChannels,0);          //All gain at -inf
    m_outMixer[m_selectedOutputChannel[0]] = 1.0f;                     //0dB
    m_outMixer[m_selectedOutputChannel[1]] = 1.0f;                     //0dB


}
void AudioEngine::startMeasure()
{

    setEngine();

    //Setting duplex stream
    m_playFrameIndex = 0;
    try
    {
      m_audioEngine.openStream( &m_outParams, &m_inParams, RTAUDIO_FLOAT64,
                      m_fs, &m_bufferFrames, &this->playsweep_rec, (void *)this );
      m_audioEngine.startStream();
    }
    catch ( RtAudioError& e )
    {
      e.printMessage();
      exit( 0 );
    }

    return;
}

void AudioEngine::startTest()
{

    setEngine();

    //Setting duplex stream
    m_playFrameIndex = 0;
    try
    {
      m_audioEngine.openStream( &m_outParams, &m_inParams, RTAUDIO_FLOAT64,
                      m_fs, &m_bufferFrames, &this->playsweep_test, (void *)this );
      m_audioEngine.startStream();
    }
    catch ( RtAudioError& e )
    {
      e.printMessage();
      exit( 0 );  // c'est peut-être un peu violent? Tu peux créer un signal "error(QString)" et emit ici pour afficher l'erreur dans l'UI ?
    }
}

void AudioEngine::stopMeasure()
{
    // Stop the recording
    m_audioEngine.stopStream();
    m_audioEngine.closeStream();
    m_isPlaying = false;
    emit isPlayingChanged(m_isPlaying);

    //levelMeter to zero
    for (int j=0; j<m_inParams.nChannels; j++ )
    {
        m_level[j] = -INFINITY;
    }
    emit levelChanged1(m_level[0]);
    emit levelChanged2(m_level[1]);

    // Call the File Dialog
    emit filenameRequested();
}

void AudioEngine::abortRecord()
{
    // Stop the recording
    m_audioEngine.stopStream();
    m_audioEngine.closeStream();
    m_isPlaying = false;
    emit isPlayingChanged(m_isPlaying);

    //levelMeter to zero
    for (int j=0; j<m_inParams.nChannels; j++ )
    {
        m_level[j] = -INFINITY;
    }
    emit levelChanged1(m_level[0]);
    emit levelChanged2(m_level[1]);
}

void AudioEngine::computeImpulseResponse(QString filename)
{

    // TODO : truncate to a power of 2 before convolution

    QVector<double> recMeasr;                      // Measure signal
    QVector<double> recRef;                        // Reference signal
    QVector<double> impulseMeasr(2*m_recSize -1);  // Impulse response of the measure
    QVector<double> impulseRef(2*m_recSize -1);    // impuse repsonse of the ref
    QVector<double> impulseFinal(2*m_recSize-1);   // Impulse response of the system


    // Extracting record samples, windowing and truncating
    for (int i=0; i< m_recSize; i++)
    {
        recMeasr.append(m_rec[m_selectedInputChannel[0]][i] * m_recWin[i]);
        recRef.append(m_rec[m_selectedInputChannel[1]][i] * m_recWin[i]);
    }

    // Computing Impulse responses
    conv(recMeasr,invsweep,impulseMeasr,ConvDirection::Convolution);
    conv(recRef,invsweep,impulseRef,ConvDirection::Convolution);


    // TODO : test if we have to do deconvolution first of not

    // When convoluting with Angelo Farina's method, the IR is offset by sweepSize
    // So we discard the first part.
    impulseMeasr = impulseMeasr.mid(m_sweepSize-1,m_recSize);
    impulseRef = impulseRef.mid(m_sweepSize-1,m_recSize);

    // Deconvolution (no offset needed here)
    if(true)
        conv(impulseMeasr, impulseRef, impulseFinal, ConvDirection::Deconvolution);
    else //not using reference signal. storing the measure without deconvolving
        impulseFinal = QVector<double>(impulseRef);

    // Testing correct extension
    int l = filename.size();
    QString ext = filename.mid(l-4,4);
    if (ext.compare(".imp") != 0 )
        filename.append(".imp");

    if(writeToFile(impulseFinal, filename))
    {
        emit measureCompleted((filename));
    };

    return;

}

bool AudioEngine::writeToFile(QVector<double> data, QString filename)
{
    // MAC version : discard "file://"
    filename = filename.mid(7);

    writeToImpulseFile(filename, data);
    return true;

    /*
    SNDFILE *file;
    SF_INFO info;
    sf_count_t idx;

    // Testing correct extension
    int l = filename.size();
    QString ext = filename.mid(l-4,4);
    if (ext.compare(".wav") != 0 )
        filename.append(".wav");

    // TODO : test if writing file is ok

    // MAC version : discard "file://"
    std::string filenm = filename.toStdString().substr(7);

    // Writing to .wav file
    info.channels = 1;
    info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;
    info.samplerate = m_fs;

    if (filenm == "")
        return false;

    // If file exist : remove it for update
    FILE* fl;
    if ((fl=fopen(filenm.c_str(),"r")) != nullptr)
    {
        fclose(fl);
        remove(filenm.c_str());
    }

    // Write to the file
    file = sf_open(filenm.c_str(),SFM_RDWR, &info);
    assert(file != NULL);
    idx = sf_write_double(file,data.data(),m_sweepSize);
    sf_close(file);

    return true;
    */

}

void AudioEngine::setInDevice(int i)
{
    if(m_selectedInputDevice == i)
        return;
    m_selectedInputDevice = i;
    emit inDeviceChanged(m_selectedInputDevice);

}


void AudioEngine::setOutDevice(int i)
{
    if(m_selectedOutputDevice == i)
        return;
    m_selectedOutputDevice = i;
    emit outDeviceChanged(m_selectedOutputDevice);

}


int AudioEngine::playsweep_rec( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
                   double streamTime, RtAudioStreamStatus status, void *userData )
{
    AudioEngine *data = static_cast<AudioEngine *>(userData);
    double *outprt = static_cast<double *>(outputBuffer);
    double *inptr = static_cast<double*>(inputBuffer);
    static int count = 0;

    // TODO : Check underflow
    if ( status )
      qDebug() << "Stream underflow detected!";

    data->m_level.clear();
    data->m_level.resize(data->m_inParams.nChannels);

    for (unsigned int i=0; i<nBufferFrames; i++ )
    {
        for (int j=0; j<data->m_inParams.nChannels; j++ )
        {
            double s = *inptr;
            // Record from all input channels
            data->m_rec[j].append(s);
            // Compute RMS level
            data->m_level[j] = data->m_level.at(j) + s*s;
            inptr++;
        }

        if(++count == RMS_LENGTH) {
            for (int j=0; j<data->m_inParams.nChannels; j++ )
            {
                data->m_level[j] = 20*log(sqrt(data->m_level.at(j) / static_cast<double>(count))) + data->m_gain;
            }
            //emit data->levelChanged(data->m_level);
            emit data->levelChanged1(data->m_level.at(0));
            emit data->levelChanged2(data->m_level.at(1));
            count = 0;
        }

        // Play the sweep in the selected output channels
        if (data->m_playFrameIndex < data->m_sweepSize)
        {
            for (int j=0; j<data->m_outParams.nChannels; j++ )
            {
                *outprt++ = data->m_outMixer[j] * (data->m_sweep[data->m_playFrameIndex]);
            }
            data->m_playFrameIndex++;
        }
        else
        {   // Sweep ended
            for (unsigned int j=0; j<data->m_outParams.nChannels; j++ )
            {
                *outprt++ = static_cast<double>(0.0f);
            }
            data->m_playFrameIndex++;
        }

        // Stop the recording ?
        if(data->m_playFrameIndex == data->m_recSize)
        {
            emit data->recordCompleted();
        }
    }
    return 0;
}

int AudioEngine::playsweep_test( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
                   double streamTime, RtAudioStreamStatus status, void *userData )
{
    AudioEngine *data = static_cast<AudioEngine *>(userData);
    double *outprt = static_cast<double *>(outputBuffer);
    double *inptr = static_cast<double*>(inputBuffer);
    static
            int count = 0;

    // TODO : Check underflow
    if ( status )
      qDebug() << "Stream underflow detected!";

    data->m_level.clear();
    data->m_level.resize(data->m_inParams.nChannels);


    for (unsigned int i=0; i<nBufferFrames; i++ )
    {
        // Compute RMS level
        for (int j=0; j<data->m_inParams.nChannels; j++ )
        {
            double s = *inptr;

            data->m_level[j] = data->m_level.at(j) + s*s;
            inptr++;
        }
        if(++count == RMS_LENGTH) {
            for (int j=0; j<data->m_inParams.nChannels; j++ )
            {
                data->m_level[j] = 20*log(sqrt(data->m_level.at(j) / static_cast<double>(count))) + data->m_gain;
            }
            //emit data->levelChanged(data->m_level);
            emit data->levelChanged1(data->m_level.at(0));
            emit data->levelChanged2(data->m_level.at(1));
            count = 0;
        }

        // Sweep ended ?
        if (data->m_playFrameIndex >= data->m_sweepSize)
        {   // Back to the beginning of the sweep : reset m_playFrameIndex
            data->m_playFrameIndex = 0;
        }

        // Play the sweep in the selected output channels forever
        for (int j=0; j<data->m_outParams.nChannels; j++ )
        {
            *outprt++ = data->m_outMixer[j] * (data->m_sweep[data->m_playFrameIndex]);
        }
        data->m_playFrameIndex++;
    }
    return 0;
}



