#include "Measure/Measure.h"
#include "Measure/MeasureCollection.h"
#include "Audio/audioEngine.h"
#include "Auth/serialprotect.h"
#include "Measure/globals.h"

#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include <QtWebView/QtWebView>

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif



    QApplication app(argc, argv);
   // QtWebView::initialize();
    QQuickStyle:: setFallbackStyle("Material");

    qmlRegisterUncreatableType<Measure>("com.WevoSoftware.Measure", 1, 0, "Measure", "Use MeasureCollection.create");
    qmlRegisterType<MeasureCollection>("com.WevoSoftware.Measure", 1, 0, "MeasureCollection");
    qmlRegisterType<AudioEngine>("com.WevoSoftware.AudioEngine", 1, 0, "AudioEngine");
    qmlRegisterType<SerialProtect>("com.WevoSoftware.SerialProtect", 1, 0, "SerialProtect");
    qmlRegisterType<Globals>("com.WevoSoftware.Globals", 1, 0, "Globals");

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();

    MeasureCollection measures;
    AudioEngine ae;
    SerialProtect srl;
    Globals globals;

    QQmlEngine::setObjectOwnership(&ae,QQmlEngine::CppOwnership);

    context->setContextProperty("globals", &globals);
    context->setContextProperty("measures", &measures);
    context->setContextProperty("ae", &ae);
    context->setContextProperty("srl", &srl);
    context->setContextProperty("globals", &globals);
    context->setContextProperty("inDeviceModel",
                                QVariant::fromValue(ae.getInDeviceList()));
    context->setContextProperty("outDeviceModel", QVariant::fromValue(ae.getOutDeviceList()));

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
    {
        return -1;
    }

    return app.exec();
}
